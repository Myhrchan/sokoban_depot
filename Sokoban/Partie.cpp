#include "Partie.h"


Partie::Partie()
{

}

Partie::~Partie()
{

}

int Partie::getNbPousse()
{
    return m_nombre_pousse;
}

void Partie::indtpousse()
{
    m_nombre_pousse++;
}

///R�initialise les variables de jeu
void Partie::reset(Niveau& niv)
{
    niv.setCheckpoint(0);
    niv.setResolu(false);
    niv.reinitGoals();
    niv.reinitTab();
    niv.setTaillex(NB_CASES_LARGEUR);
    niv.setTailley(NB_CASES_HAUTEUR);
}

void Partie::boucleJeu(Allegro& alleg)
{

    Niveau niv;
    Niveau& nivmodif = niv;
    Heros mario;
    Heros& Rmario = mario;
    Maze maze;
    bool detection_deadlock = true;
    int mode = 0;
    int posx, posy;
    std::string nom_niveau;
    bool mouvement_possible=true; // boullean a vrai tant que le mouvement est possible ( recup�re le retour de l'algo)
    std::vector<std::vector<int> > matrice;
    std::vector<std::vector<int> > goals;

    while(!key[KEY_ESC])
    {
        nom_niveau = " ";
        nom_niveau = alleg.choixNiveau();
        mode = alleg.choixMode(&detection_deadlock);
        reset(nivmodif);
        mouvement_possible=true;
        if(!key[KEY_ESC])
        {
            nivmodif.chargementNiveau(&posx, &posy, nom_niveau, mode);
            mario.setPosx(posx);
            mario.setPosy(posy);
            if(detection_deadlock == true) maze.detectionDeadlocks(nivmodif);

            while(nivmodif.getResolu() != true && !key[KEY_ESC] && mouvement_possible==true)
            {
                matrice = nivmodif.getMatrice();

                goals = nivmodif.getGoals();
                switch(mode)
                {
                    case 1: /// jeu manuel
                        alleg.affichageMatrice(matrice, mario, niv, 0);
                        mario.diriger_Mario(4);
                        mario.collisionsMario(nivmodif);
                        break;
                    case 2: /// force brute
                        alleg.affichageMatrice(matrice, mario, niv, 0);
                        mouvement_possible=maze.recherche_naif(Rmario,nivmodif);
                        break;
                    case 3: /// BFS
                        maze.BFS(nivmodif,mario, alleg);
                        nivmodif.setResolu(1);
                        break;
                    case 4: /// DFS
                        maze.DFS(nivmodif,mario, alleg);
                        nivmodif.setResolu(1);
                        break;
                    case 5: /// Best FS
                        maze.Best_First_Search(nivmodif,mario, alleg);
                        nivmodif.setResolu(1);
                        break;
                    case 6: /// Astar
                        maze.Astar(nivmodif,mario, alleg);
                        nivmodif.setResolu(1);
                        break;

                }

                std::cout<<"Px : " << mario.getPosx() <<"Py : " <<mario.getPosy() <<std::endl;

                ///On regarde le nombre de checkpoints valid�s
                nivmodif.indtcp();
                if(nivmodif.getResolu() == true && (mode==1 || mode==2)) alleg.affichageMatrice(matrice, mario, niv, 1);
            }
            while(key[KEY_ESC]);
        }
    }
}
