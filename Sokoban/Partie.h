#ifndef PARTIE_H_INCLUDED
#define PARTIE_H_INCLUDED
#include "header.h"
#include "init.h"
#include "Niveau.h"
#include "Heros.h"
#include "Maze.h"

class Partie
{
private :
    int m_nombre_pousse; // compte le nombre de bloc que le joeur a pouss�

public:
    Partie();
    ~Partie();
    int getNbPousse();
    void indtpousse();

    /// Boucle de jeu
    void boucleJeu(Allegro& alleg);
    void reset(Niveau& niv);
};

#endif // PARTIE_H_INCLUDED
