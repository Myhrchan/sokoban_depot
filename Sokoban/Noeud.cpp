#include "Noeud.h"


// constructeur par defaut
Noeud::Noeud(const std::vector <std::vector <int> > & vect, std::vector<std::vector<int> > n_zone_accessible) : m_pos_box(vect)
{
    m_Parent=NULL;
    m_direction=-1;
    m_zone_accessible=n_zone_accessible;
    m_index =-1;
    m_poid=0;
    m_position_normalisee=0;
    m_profondeur=0;
}

//constructeur par surchage non pond�r�
Noeud::Noeud(const std::vector <std::vector <int> > & vect,std::vector<std::vector<int> > n_zone_accessible,Noeud* n_Parent, int n_direction,int n_index) : m_pos_box(vect)
{
    const std::vector <std::vector <int> > & tempo(vect);
    m_Parent=n_Parent;
    m_direction=n_direction;
    m_zone_accessible=n_zone_accessible;
    m_pos_box= tempo; // on copie la matrice de position des box pour eviter de modifier l'original
    m_index =n_index;
    m_poid=0;
    m_position_normalisee=0;
    m_profondeur=0;


    //std::cout<<"n_direction = " << n_direction <<std::endl;

    // on change la position de la caisse d�plac� ( on a pas besoin de blinder car on l'a deja fait avant de rajouter le noeud)
        if (n_direction == 0) // haut
        {
            m_pos_box[n_index][0]=  m_pos_box[n_index][0] -1;
        }
        else if (n_direction == 1) // bas
        {
            m_pos_box[n_index][0]=  m_pos_box[n_index][0] +1;
        }
        else if (n_direction == 2) // gauche
        {
            m_pos_box[n_index][1]=  m_pos_box[n_index][1] -1;

        }
        else if (n_direction == 3) // droite
        {
           m_pos_box[n_index][1]=  m_pos_box[n_index][1] +1;
          //std::cout<<"LA CAISE BOUGE !!!!" <<std::endl;
        }

}



//constructeur par surchage pond�r�
Noeud::Noeud(const std::vector <std::vector <int> > & vect,std::vector<std::vector<int> > n_zone_accessible,Noeud* n_Parent, int n_direction,int n_index,Niveau& niv) : m_pos_box(vect)
{
    const std::vector <std::vector <int> > & tempo(vect);
    m_Parent=n_Parent;
    m_direction=n_direction;
    m_zone_accessible=n_zone_accessible;
    m_pos_box= tempo; // on copie la matrice de position des box pour eviter de modifier l'original
    m_index =n_index;
    m_poid=calcul_heuristique(niv);
    m_position_normalisee=0;
    m_profondeur=0;

    //std::cout<<"n_direction = " << n_direction <<std::endl;

    // on change la position de la caisse d�plac� ( on a pas besoin de blinder car on l'a deja fait avant de rajouter le noeud)
        if (n_direction == 0) // haut
        {
            m_pos_box[n_index][0]=  m_pos_box[n_index][0] -1;
        }
        else if (n_direction == 1) // bas
        {
            m_pos_box[n_index][0]=  m_pos_box[n_index][0] +1;
        }
        else if (n_direction == 2) // gauche
        {
            m_pos_box[n_index][1]=  m_pos_box[n_index][1] -1;

        }
        else if (n_direction == 3) // droite
        {
           m_pos_box[n_index][1]=  m_pos_box[n_index][1] +1;
          //std::cout<<"LA CAISE BOUGE !!!!" <<std::endl;
        }

}


Noeud::Noeud( Noeud const& NoeudACopier) : m_pos_box(NoeudACopier.m_pos_box)
{
        m_Parent = NoeudACopier.m_Parent;
        m_direction= NoeudACopier.m_direction;
        m_index= NoeudACopier.m_index;
        m_zone_accessible= NoeudACopier.m_zone_accessible;
        m_poid=NoeudACopier.m_poid;
        m_position_normalisee=0;
        m_profondeur=0;
}

int Noeud::get_m_profondeur()
{
    return m_profondeur;
}

void Noeud::set_m_profondeur(int i)
{
    m_profondeur = i;
}

int Noeud::get_m_index()
{
    return m_index;
}

void Noeud::set_m_poid(int i)
{
    m_poid += i;
}

void Noeud::set_m_position_normalisee(int valeur)
{
    m_position_normalisee = valeur;
}

int Noeud::get_m_position_normalisee()
{
    return m_position_normalisee;
}

int Noeud::get_m_direction()
{
    return m_direction;
}

std::vector<int> Noeud::getBox(unsigned int indice)
{
    std::vector<int> tempotab;
    if(indice < m_pos_box.size() && indice >= 0)
    {
         tempotab.push_back(m_pos_box[indice][0]);
         tempotab.push_back(m_pos_box[indice][1]);
         return tempotab;
    }
    return (std::vector<int>()); // on retourne un vecteur vide
}

Noeud* Noeud::get_Parent()
{
    return(m_Parent);
}


bool Noeud::noeud_solution(Niveau& niv) // sous rogramme auquel on envoi un noeud et qui verifie si il est solution au jeu
{

    int i;
    bool verif =1;
    for(i=0;i<niv.getNbGoals();i++)
    {
        //std::cout<<"(m_pos_box[i][1] = " << m_pos_box[i][1]<<" m_pos_box[i][0] = "<<m_pos_box[i][0]<<" niv.getCaseMatrice(1,6) = "<< niv.getCaseMatrice(1,6)<<std::endl  ;
        if (niv.getCaseMatrice(m_pos_box[i][0],m_pos_box[i][1]) != 4 ) // on regarde si on est pas sur un chekpoint si on a une caiss qui n'es pas sur
        {
           verif =0;
        }
    }
    return verif;
}

std::vector <std::vector <int> >  Noeud::get_m_box()
{
    return m_pos_box;
}

bool Noeud::getAccessible(int h, int l)
{
    //std::cout<<std::endl<<"h = "<<h<<" L = " <<l<<" m_zone_accessible[h][l]"<<m_zone_accessible[h][l]<<std::endl;
    if(m_zone_accessible[h][l] == 1) return true;
    return false;
}


std::vector<std::vector <int> > Noeud:: getm_zone_accessible()
{
    return m_zone_accessible;
}


Noeud::~Noeud()
{

}

Noeud& Noeud::operator=(Noeud const& NoeudACopier)
{
    if(this != &NoeudACopier)
    //On v�rifie que l'objet n'est pas le m�me que celui re�u en argument
    {
        m_Parent = NoeudACopier.m_Parent;
        m_pos_box= NoeudACopier.m_pos_box;
        m_direction= NoeudACopier.m_direction;
        m_index= NoeudACopier.m_index;
        m_zone_accessible= NoeudACopier.m_zone_accessible;
    }
    return *this; //On renvoie l'objet lui-m�me
}

void Noeud::setm_accessible(std::vector<std::vector <int> > n_vect)
{
    m_zone_accessible = n_vect;
}

bool Noeud::is_box(int y,int x, Niveau& niv) // fonction qui nous renvoi 1 si il y a une boite a cette endroit et 0 sinon
{

    for(int i=0; i<niv.getNbBox(); i++)
    {
                if ( y ==  m_pos_box[i][0] &&  x == m_pos_box[i][1] ) return 1;

    }
    return 0;
}

int Noeud::get_m_poid()
{
    return m_poid;
}

///renvoie un entier correspondant � la distance entre la caisse consid�r�e et le goal le plus proche
///En entr�e : niveau + les coordonn�es de la caisse � traiter dans un vecteur
int Noeud::calcul_heuristique( Niveau& niv)
{
    std::vector< std::vector<int> > goals = niv.getGoals();
    int heuristique = 0;
    for (int j=0;j<niv.getNbGoals();j++)
    {
        for(int i=0; i<niv.getNbBox(); i++)
        { ///on fait la somme des valeurs absolues des distances entre le goal et la caisse
             heuristique = fabs(m_pos_box[i][0]-goals[j][0]) + fabs(m_pos_box[i][1]-goals[j][1]);
        }
    }

    return heuristique;
}
