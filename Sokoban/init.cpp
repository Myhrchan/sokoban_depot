#include "init.h"

Allegro::Allegro()
{
    m_personnage.resize(4);
    m_blocs.resize(4);
}

Allegro::~Allegro()
{
    destroy_bitmap(m_buffer);
}

/// Initialisation d'Allegro
void Allegro::allegroInit()
{
    allegro_init();
    install_keyboard();
    install_mouse();
    set_color_depth(16);

    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, LARGEUR_FENETRE, HAUTEUR_FENETRE, 0, 0)!=0)
    {
        allegro_message("Probleme mode graphique : %s", allegro_error);
        allegro_exit();
        exit(EXIT_FAILURE);
    }
    show_mouse(screen);

    /// Cr�ation du buffer
    m_buffer=create_bitmap(SCREEN_W, SCREEN_H);
}


/// Charge une image bitmap
BITMAP* Allegro::chargerImage(std::string nomFichierImage)
{
    BITMAP *bmp = NULL;
    bmp=load_bitmap(nomFichierImage.c_str(),NULL);
    if (bmp==NULL)
    {
        std::cout << "Fichier introuvable : " << nomFichierImage << std::endl;
        allegro_exit();
        exit(EXIT_FAILURE);
    }
    std::cout << "Image charg\202e : " << nomFichierImage << std::endl;
    return bmp;
}


///Charge la totalit� des images du jeu
void Allegro::loadAllImages()
{
    BITMAP* temp = NULL;
    std::string chemin;

    /// Chargement des images des murs et des blocs
    chemin = "images/mur.bmp";
    temp = chargerImage(chemin);
    m_blocs[0] = temp;

    chemin = "images/bloc1.bmp";
    temp = chargerImage(chemin);
    m_blocs[1] = temp;

    chemin = "images/bloc2.bmp";
    temp = chargerImage(chemin);
    m_blocs[2] = temp;

    chemin = "images/goal.bmp";
    temp = chargerImage(chemin);
    m_blocs[3] = temp;

    /// Chargement des images du personnage
    chemin = "images/mario1.bmp";
    temp = chargerImage(chemin);
    m_personnage[0] = temp;

    chemin = "images/mario2.bmp";
    temp = chargerImage(chemin);
    m_personnage[1] = temp;

    chemin = "images/mario3.bmp";
    temp = chargerImage(chemin);
    m_personnage[2] = temp;

    chemin = "images/mario4.bmp";
    temp = chargerImage(chemin);
    m_personnage[3] = temp;
}

///affichage du choix des niveaux
///sortie : la chaine de caract�res correspondant au niveau demand�
std::string Allegro::choixNiveau()
{
    std::string niveau = " ";
    std::string temp = "";

    clear(m_buffer);
    rectfill(m_buffer,0,0,SCREEN_W,SCREEN_H, makecol(255,255,255));
    textout_ex(m_buffer, font, "MENU", 380, 20, makecol(0,0,0), -1);

    textout_ex(m_buffer, font, "TEST", 380, 80, makecol(0,0,0), -1);
    line(m_buffer, SCREEN_W/2-75, 100, SCREEN_W/2+75, 100, makecol(0,0,0));
    line(m_buffer, SCREEN_W/2-75, 130, SCREEN_W/2+75, 130, makecol(0,0,0));
    line(m_buffer, SCREEN_W/2-75, 100, SCREEN_W/2-75, 130, makecol(0,0,0));
    line(m_buffer, SCREEN_W/2+75, 100, SCREEN_W/2+75, 130, makecol(0,0,0));
    line(m_buffer, SCREEN_W/2-25, 100, SCREEN_W/2-25, 130, makecol(0,0,0));
    line(m_buffer, SCREEN_W/2+25, 100, SCREEN_W/2+25, 130, makecol(0,0,0));
    textout_ex(m_buffer, font, "1", SCREEN_W/2-55, 110, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "2", SCREEN_W/2-5, 110, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "3", SCREEN_W/2+50, 110, makecol(0,0,0), -1);



    textout_ex(m_buffer, font, "EASY", 380, 170, makecol(0,0,0), -1);
    line(m_buffer, SCREEN_W/2-50*5, 190, SCREEN_W/2+50*5, 190, makecol(0,0,0));
    line(m_buffer, SCREEN_W/2-50*5, 220, SCREEN_W/2+50*5, 220, makecol(0,0,0));
    for(signed int i=-5; i<6; i++)
        line(m_buffer, SCREEN_W/2+50*i, 190, SCREEN_W/2+50*i, 220, makecol(0,0,0));
    textout_ex(m_buffer, font, "1", SCREEN_W/2-50*5+25, 200, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "2", SCREEN_W/2-50*4+25, 200, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "3", SCREEN_W/2-50*3+25, 200, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "4", SCREEN_W/2-50*2+25, 200, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "5", SCREEN_W/2-25, 200, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "6", SCREEN_W/2+25, 200, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "7", SCREEN_W/2+50*2-25, 200, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "8", SCREEN_W/2+50*3-25, 200, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "9", SCREEN_W/2+50*4-25, 200, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "10", SCREEN_W/2+50*5-25, 200, makecol(0,0,0), -1);


    textout_ex(m_buffer, font, "MEDIUM", 370, 260, makecol(0,0,0), -1);
    line(m_buffer, SCREEN_W/2-75, 280, SCREEN_W/2+75, 280, makecol(0,0,0));
    line(m_buffer, SCREEN_W/2-75, 310, SCREEN_W/2+75, 310, makecol(0,0,0));
    line(m_buffer, SCREEN_W/2-75, 280, SCREEN_W/2-75, 310, makecol(0,0,0));
    line(m_buffer, SCREEN_W/2+75, 280, SCREEN_W/2+75, 310, makecol(0,0,0));
    line(m_buffer, SCREEN_W/2-25, 280, SCREEN_W/2-25, 310, makecol(0,0,0));
    line(m_buffer, SCREEN_W/2+25, 280, SCREEN_W/2+25, 310, makecol(0,0,0));
    textout_ex(m_buffer, font, "1", SCREEN_W/2-55, 290, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "2", SCREEN_W/2-5, 290, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "3", SCREEN_W/2+50, 290, makecol(0,0,0), -1);

    textout_ex(m_buffer, font, "HARD", 380, 360, makecol(0,0,0), -1);
    line(m_buffer, SCREEN_W/2-75, 380, SCREEN_W/2+75, 380, makecol(0,0,0));
    line(m_buffer, SCREEN_W/2-75, 410, SCREEN_W/2+75, 410, makecol(0,0,0));
    line(m_buffer, SCREEN_W/2-75, 380, SCREEN_W/2-75, 410, makecol(0,0,0));
    line(m_buffer, SCREEN_W/2+75, 380, SCREEN_W/2+75, 410, makecol(0,0,0));
    line(m_buffer, SCREEN_W/2-25, 380, SCREEN_W/2-25, 410, makecol(0,0,0));
    line(m_buffer, SCREEN_W/2+25, 380, SCREEN_W/2+25, 410, makecol(0,0,0));
    textout_ex(m_buffer, font, "1", SCREEN_W/2-55, 390, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "2", SCREEN_W/2-5, 390, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "3", SCREEN_W/2+50, 390, makecol(0,0,0), -1);

    blit(m_buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H);

    while(niveau == " " && !key[KEY_ESC])
    {
        if(mouse_b&1)
        {
            ///si on clique sur un niveau test
            if(mouse_y<=130 && mouse_y >=100)
            {
                if(mouse_x>SCREEN_W/2-75 && mouse_x < SCREEN_W/2-25) niveau = "test/test_1.txt";
                if(mouse_x>SCREEN_W/2-25 && mouse_x < SCREEN_W/2+25) niveau = "test/test_2.txt";
                if(mouse_x>SCREEN_W/2+25 && mouse_x < SCREEN_W/2+75) niveau = "test/test_3.txt";
            }

            ///si on clique sur un niveau easy
            if(mouse_y<=220 && mouse_y >=190)
            {
                if(mouse_x>SCREEN_W/2-50*5 && mouse_x < SCREEN_W/2-50*4) niveau = "easy/easy_1.txt";
                if(mouse_x>SCREEN_W/2-50*4 && mouse_x < SCREEN_W/2-50*3) niveau = "easy/easy_2.txt";
                if(mouse_x>SCREEN_W/2-50*3 && mouse_x < SCREEN_W/2-50*2) niveau = "easy/easy_3.txt";
                if(mouse_x>SCREEN_W/2-50*2 && mouse_x < SCREEN_W/2-50) niveau = "easy/easy_4.txt";
                if(mouse_x>SCREEN_W/2-50 && mouse_x < SCREEN_W/2) niveau = "easy/easy_5.txt";
                if(mouse_x>SCREEN_W/2 && mouse_x < SCREEN_W/2+50) niveau = "easy/easy_6.txt";
                if(mouse_x>SCREEN_W/2+50 && mouse_x < SCREEN_W/2+50*2) niveau = "easy/easy_7.txt";
                if(mouse_x>SCREEN_W/2+50*2 && mouse_x < SCREEN_W/2+50*3) niveau = "easy/easy_8.txt";
                if(mouse_x>SCREEN_W/2+50*3 && mouse_x < SCREEN_W/2+50*4) niveau = "easy/easy_9.txt";
                if(mouse_x>SCREEN_W/2+50*4 && mouse_x < SCREEN_W/2+50*5) niveau = "easy/easy_10.txt";
            }

            ///si on clique sur un niveau medium
            if(mouse_y<=310 && mouse_y >=280)
            {
                if(mouse_x>SCREEN_W/2-75 && mouse_x < SCREEN_W/2-25) niveau = "medium/medium_1.txt";
                if(mouse_x>SCREEN_W/2-25 && mouse_x < SCREEN_W/2+25) niveau = "medium/medium_2.txt";
                if(mouse_x>SCREEN_W/2+25 && mouse_x < SCREEN_W/2+75) niveau = "medium/medium_3.txt";
            }

            ///si on clique sur un niveau hard
            if(mouse_y<=410 && mouse_y >=380)
            {
                if(mouse_x>SCREEN_W/2-75 && mouse_x < SCREEN_W/2-25) niveau = "hard/hard_1.txt";
                if(mouse_x>SCREEN_W/2-25 && mouse_x < SCREEN_W/2+25) niveau = "hard/hard_2.txt";
                if(mouse_x>SCREEN_W/2+25 && mouse_x < SCREEN_W/2+75) niveau = "hard/hard_3.txt";
            }
            while(mouse_b&1);
        }
    }
    return niveau;
}

/// choix du mode de r�alisation du niveau (manuel, force brute, DFS ...)
/// entr�e : un pointeur sur bool pour savoir si on d�tecte les deadlocks ou non
/// sortie : le num�ro correspondant au mode
int Allegro::choixMode(bool* detection_deadlock)
{
    clear(m_buffer);
    rectfill(m_buffer,0,0,SCREEN_W,SCREEN_H, makecol(255,255,255));

    int mode = 0;
    bool ok = false;

    textout_ex(m_buffer, font, "CHOIX DU MODE ", SCREEN_W/2-50, 25, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "DETECTION DES DEADLOCKS ", 250, 100, makecol(0,0,0), -1);

    rect(m_buffer, SCREEN_W/2+100, 75, SCREEN_W/2+150, 125,  makecol(0,0,0));
    if(*detection_deadlock == true)
    {
        line(m_buffer, SCREEN_W/2+100, 75, SCREEN_W/2+150, 125,  makecol(255,0,0));
        line(m_buffer, SCREEN_W/2+150, 75, SCREEN_W/2+100, 125,  makecol(255,0,0));
    }

    rect(m_buffer, 100, 200, SCREEN_W/2-50, 230,  makecol(0,0,0));
    rect(m_buffer, SCREEN_W-100, 250, SCREEN_W/2+50, 280,  makecol(0,0,0));
    rect(m_buffer, 100, 250, SCREEN_W/2-50, 280,  makecol(0,0,0));
    rect(m_buffer, SCREEN_W-100, 300, SCREEN_W/2+50, 330,  makecol(0,0,0));
    rect(m_buffer, 100, 300, SCREEN_W/2-50, 330,  makecol(0,0,0));
    rect(m_buffer, SCREEN_W-100, 200, SCREEN_W/2+50, 230,  makecol(0,0,0));


    textout_ex(m_buffer, font, "JEU MANUEL", SCREEN_W/2-230, 210, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "FORCE BRUTE", SCREEN_W/2+140, 210, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "BFS", SCREEN_W/2-210, 260, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "DFS", SCREEN_W/2+160, 260, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "BEST FIRST SEARCH", SCREEN_W/2-260, 310, makecol(0,0,0), -1);
    textout_ex(m_buffer, font, "ASTAR", SCREEN_W/2+160, 310, makecol(0,0,0), -1);

    blit(m_buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H);

    while(ok == false && !key[KEY_ESC])
    {
        if(mouse_b&1)
        {
            /// Activation de la d�tection des deadlocks
            if(mouse_x<SCREEN_W/2+150 && mouse_x>SCREEN_W/2+100 && mouse_y<125 && mouse_y>75)
            {
                *detection_deadlock = !(*detection_deadlock);
                if(*detection_deadlock == true)
                {
                    line(m_buffer, SCREEN_W/2+100, 75, SCREEN_W/2+150, 125,  makecol(255,0,0));
                    line(m_buffer, SCREEN_W/2+150, 75, SCREEN_W/2+100, 125,  makecol(255,0,0));
                }
                else if(*detection_deadlock == false)
                {
                    rectfill(m_buffer, SCREEN_W/2+100, 75, SCREEN_W/2+150, 125,  makecol(255,255,255));
                    rect(m_buffer, SCREEN_W/2+100, 75, SCREEN_W/2+150, 125,  makecol(0,0,0));
                }
                blit(m_buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
                while(mouse_b&1);

            }

            ///Jeu manuel
            if(mouse_x>100 && mouse_y>200 && mouse_x<SCREEN_W/2-50 && mouse_y<230)
            {
                mode = 1;
                ok = true;
            }

            ///Force brute
            if(mouse_x<SCREEN_W-100 && mouse_y>200 && mouse_x>SCREEN_W/2+50 && mouse_y<230)
            {
                mode = 2;
                ok = true;
            }

            ///BFS
            if(mouse_x<SCREEN_W/2-50 && mouse_y>250 && mouse_x>100 && mouse_y<280)
            {
                mode = 3;
                ok = true;
            }

            ///DFS
            if(mouse_x<SCREEN_W-100 && mouse_y>250 && mouse_x>SCREEN_W/2+50 && mouse_y<280)
            {
                mode = 4;
                ok = true;
            }

            ///Best First Search
            if(mouse_x<SCREEN_W/2-50 && mouse_y>300 && mouse_x>100 && mouse_y<330)
            {
                mode = 5;
                ok = true;
            }

            ///Astar
            if(mouse_x<SCREEN_W-100 && mouse_y>300 && mouse_x>SCREEN_W/2+50 && mouse_y<330)
            {
                mode = 6;
                ok = true;
            }
        }
    }
    return mode;
}

/// affichage de la totalit� de la matrice � l'�cran
void Allegro::affichageMatrice(std::vector<std::vector<int> > matrice, Heros& mario, Niveau& niv, int fin)
{
    std::vector<std::vector<int> > goals = niv.getGoals();
    clear(m_buffer);
    rectfill(m_buffer,0,0,SCREEN_W,SCREEN_H, makecol(255,255,255));
    rectfill(m_buffer,65,35,SCREEN_W-70,SCREEN_H-40, makecol(240,180,160));

    ///affichages des boites et murs
    if(fin == 0) //si le jeu n'est pas fini
        for(int i=0; i<niv.getTailley(); i++)
        {
            for(int j=0; j<niv.getTaillex(); j++)
            {
                if(matrice[i][j] >= 1 && matrice[i][j] <= 4)
                    draw_sprite(m_buffer, m_blocs[matrice[i][j]-1], 66+35*j, 36+35*i);
                if(matrice[i][j] == 0)
                    rectfill(m_buffer, 66+35*j, 35+35*i, 66+35*j+33, 35+35+35*i, makecol(255,255,255));
            }
        }

    if(fin == 1) //si le jeu est fini
        for(int i=0; i<niv.getTailley(); i++)
        {
            for(int j=0; j<niv.getTaillex(); j++)
            {
                if(matrice[i][j] == 1)
                    draw_sprite(m_buffer, m_blocs[matrice[i][j]-1], 66+35*j, 36+35*i);
                if(matrice[i][j] == 0 || matrice[i][j] == 2)
                    rectfill(m_buffer, 66+35*j, 35+35*i, 66+35*j+33, 35+35+35*i, makecol(255,255,255));
            }
        }

    ///affichage des goals
    for(int i=0; i<niv.getNbGoals(); i++)
    {
        if(fin == 0 && (matrice[goals[i][0]][goals[i][1]] == 0 || matrice[goals[i][0]][goals[i][1]] == 6 || matrice[goals[i][0]][goals[i][1]] == 4))
        {
            rectfill(m_buffer, 66+35*goals[i][1], 35+35*goals[i][0], 66+35*goals[i][1]+33, 35+35+35*goals[i][0], makecol(255,255,255));
            draw_sprite(m_buffer, m_blocs[3], 66+35*goals[i][1], 36+35*goals[i][0]);
        }
        if(fin ==1)
        {
            draw_sprite(m_buffer, m_blocs[2], 66+35*goals[i][1], 36+35*goals[i][0]);
        }
    }

    ///affichage du personnage
    if(mario.getVx() == 1)
        draw_sprite(m_buffer, m_personnage[1], 72+35*mario.getPosx(), 36+35*mario.getPosy());
    else if(mario.getVx() == -1)
        draw_sprite(m_buffer, m_personnage[2], 72+35*mario.getPosx(), 36+35*mario.getPosy());
    else if(mario.getVy() == 1)
        draw_sprite(m_buffer, m_personnage[0], 72+35*mario.getPosx(), 36+35*mario.getPosy());
    else if(mario.getVy() == -1)
        draw_sprite(m_buffer, m_personnage[3], 72+35*mario.getPosx(), 36+35*mario.getPosy());
    else if(mario.getVy() == 0 && mario.getVy() == 0)
        draw_sprite(m_buffer, m_personnage[0], 72+35*mario.getPosx(), 36+35*mario.getPosy());

    ///dessin de la matrice � l'�cran
    for(int i=0; i<=NB_CASES_LARGEUR; i++)
    {
        line(m_buffer, 30+35*(i+1), 35, 30+35*(i+1), 560, makecol(0,0,0));
    }
    for(int j=0; j<=NB_CASES_HAUTEUR; j++)
    {
        line(m_buffer, 65, 35*(j+1), 730, 35*(j+1), makecol(0,0,0));
    }

    blit(m_buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
    rest(150);
}


/// Affichage de la matrice fonctionnant avec les diff�rents algorithmes
void Allegro::affichage_Algo(Noeud* noeud, Niveau& niv, Heros& mario)
{
    std::vector<std::vector <int> > matrice_tempo =niv.getMatrice();
    std::queue <int> file;
    std::stack <int> pile;
    int dx=0,dy=0;
    /* on lin�arise marquage et pred pour que se soit plus facile a utiliser dans le BFS
        Pour lin�ariser on utilise les calculs suivant :

        - Matriciel -> lin�aire :
                indice lin�aire (int) = x+y*taille ligne matrice

        - Lin�aire -> matriciel =
                x= indice lin�aire%taille d'une ligne
                y= indice lineaire / taille d'une ligne ( division euclidienne)

        L'indice lin�aire sert a retrouver plus facilement les Pred avec le BFS alors que l'indice matriciel sert pour l'affichage

    */
    std::vector<int> marquage;
    std::vector<int> pred;
    marquage.resize(matrice_tempo.size()*matrice_tempo[0].size());
    pred.resize(matrice_tempo.size()*matrice_tempo[0].size());
    int tempolin=-1;
    int tempo_y=0;
    int tempo_x=0;
    int tempo_caisse_x=0;
    int tempo_caisse_y=0;

    std::vector <std::vector <int> > box = noeud->get_m_box();

    // on initialise le marquage a 0 et les pred a -1
   //std::cout<<std::endl<<"AFFICHAGE 1"<<std::endl;
    for (unsigned int i =0; i<marquage.size(); i++)
    {
        marquage[i]=0;
        pred[i]=-1;
    }

   // std::cout<<std::endl<<"AFFICHAGE 2"<<std::endl;

    clear(m_buffer);


    // on g�re les direction plus simplement grace a 2 variables x et y

    if (noeud->get_m_direction() == 0) // haut
    {
        dx=0;
        dy=-1;
    }
    else if (noeud->get_m_direction() == 1) // bas
    {
        dx=0;
        dy=1;
    }
    else if (noeud->get_m_direction() == 2) // gauche
    {
        dx=-1;
        dy=0;

    }
    else if (noeud->get_m_direction() == 3) // droite
    {
        dx=1;
        dy=0;
    }

        // on rajoute les box sur la matrice tempo ( plus simple pour le BFS )
    for (int i =0; i<niv.getNbBox(); i++)
    {
        tempo_caisse_x=box[i][1];
        tempo_caisse_y=box[i][0];

        if (i==noeud->get_m_index())
        {
            tempo_caisse_x=box[i][1]-dx;
            tempo_caisse_y=box[i][0]-dy;
        }

        matrice_tempo[tempo_caisse_y][tempo_caisse_x] = 3;
    }


    // on commence par afficher l'ensemeble de la matrice de Jeu + mario a sa position de base + les caisses � leur position dans le noeud ( sauf celle qui est d�plac�e qu'on affiche a sa position avant que le d�placement est lieu )
    rectfill(m_buffer,0,0,SCREEN_W,SCREEN_H, makecol(255,255,255));
    rectfill(m_buffer,65,35,SCREEN_W-70,SCREEN_H-40, makecol(240,180,160));

    ///affichages murs et goals (les goals sont localis�s par 4 sur la matrice)
    for(int i=0; i<niv.getTailley(); i++)
    {
        for(int j=0; j<niv.getTaillex(); j++)
        {
            if(niv.getCaseMatrice(i, j) == 0) ///rien
                rectfill(m_buffer, 66+35*j, 35+35*i, 66+35*j+33, 35+35+35*i, makecol(255,255,255));
            else if(niv.getCaseMatrice(i, j) == 1) ///murs
                draw_sprite(m_buffer, m_blocs[0], 66+35*j, 36+35*i);
            else if(niv.getCaseMatrice(i, j) == 4)
            {
                rectfill(m_buffer, 66+35*j, 35+35*i, 66+35*j+33, 35+35+35*i, makecol(255,255,255));
                draw_sprite(m_buffer, m_blocs[3], 66+35*j, 36+35*i);
            }
        }
    }

    /// Affichage des caisses
    for(unsigned int i=0; i<box.size(); i++)
    {
         tempo_caisse_x=box[i][1];
         tempo_caisse_y=box[i][0];

        if (i==noeud->get_m_index())
        {
            tempo_caisse_x=box[i][1]-dx;
            tempo_caisse_y=box[i][0]-dy;
        }


        ///si la case est vide on affiche la boite normale
        if(niv.getCaseMatrice(tempo_caisse_y, tempo_caisse_x) == 0 || niv.getCaseMatrice(tempo_caisse_y, tempo_caisse_x) == 6)
            draw_sprite(m_buffer, m_blocs[1], 66+35*tempo_caisse_x, 36+35*tempo_caisse_y);
        ///si il y a un goal on affiche une boite plac�e
        else if(niv.getCaseMatrice(tempo_caisse_y, tempo_caisse_x) == 4)
            draw_sprite(m_buffer, m_blocs[2], 66+35*tempo_caisse_x, 36+35*tempo_caisse_y);
    }

    ///dessin de la matrice � l'�cran
    for(int i=0; i<=NB_CASES_LARGEUR; i++)
    {
        line(m_buffer, 30+35*(i+1), 35, 30+35*(i+1), 560, makecol(0,0,0));
    }
    for(int j=0; j<=NB_CASES_HAUTEUR; j++)
    {
        line(m_buffer, 65, 35*(j+1), 730, 35*(j+1), makecol(0,0,0));
    }

    draw_sprite(m_buffer, m_personnage[1], 72+35* (mario.getPosx()), 36+35*(mario.getPosy()));
    blit(m_buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H);         /// premier affichage avant tout d�placement ( on reproduit juste l'affichage pr�c�dent)


// si il y a une caisse qui est pouss�e on va aller reproduire le mouvement

/// premi�re �tape : on d�place mario jusqu'a la case ou il va commencer la pouss�e
    if (noeud->get_m_index() != -1)
    {
        marquage[mario.getPosx()+((mario.getPosy())*matrice_tempo[0].size())] =1;
        file.push(mario.getPosx()+((mario.getPosy())*matrice_tempo[0].size()));


        while(file.size() !=0 && ( tempo_x != (box[noeud->get_m_index()][1]-2*dx)  || tempo_y != (box[noeud->get_m_index()][0]-2*dy)))
        {

            //std::cout << "OBJECTIF = "<< (box[noeud->get_m_index()][1]-2*dx) << "  "<<(box[noeud->get_m_index()][0]-2*dy);
            tempolin=file.front();
            file.pop();
            tempo_x=tempolin%matrice_tempo[0].size();
            tempo_y=tempolin / matrice_tempo[0].size();
            //std::cout<<std::endl<<"tempolin = "<< tempolin<<" tempo_x = "<<tempo_x<<" tempo_y = "<<tempo_y<<std::endl;

            //haut
            if( (matrice_tempo[tempo_y-1][tempo_x]==0 || matrice_tempo[tempo_y-1][tempo_x]==4 || matrice_tempo[tempo_y-1][tempo_x]==6) && (marquage[(tempo_y-1)*matrice_tempo[0].size()+tempo_x]==0) )
            {
                //std::cout<<std::endl<<"AFFICHAGE 4 h"<<std::endl;
                marquage[(tempo_y-1)*matrice_tempo[0].size()+tempo_x]= 1;
                pred[(tempo_y-1)*matrice_tempo[0].size()+tempo_x]=tempolin;
                file.push((tempo_y-1)*matrice_tempo[0].size()+tempo_x);
            }
            //bas
            if( (matrice_tempo[tempo_y+1][tempo_x]==0 || matrice_tempo[tempo_y+1][tempo_x]==4 || matrice_tempo[tempo_y+1][tempo_x]==6) && (marquage[(tempo_y+1)*matrice_tempo[0].size()+tempo_x]==0))
            {
                //std::cout<<std::endl<<"AFFICHAGE 4 b"<<std::endl;
                marquage[(tempo_y+1)*matrice_tempo[0].size()+tempo_x]= 1;
                pred[(tempo_y+1)*matrice_tempo[0].size()+tempo_x]=tempolin;
                file.push((tempo_y+1)*matrice_tempo[0].size()+tempo_x);
            }
            //qauche
            if( (matrice_tempo[tempo_y][tempo_x-1]==0 || matrice_tempo[tempo_y][tempo_x-1]==4 || matrice_tempo[tempo_y][tempo_x-1]==6) && (marquage[(tempo_y)*matrice_tempo[0].size()+tempo_x-1]==0))
            {
                //std::cout<<std::endl<<"AFFICHAGE 4 g"<<std::endl;
                marquage[(tempo_y)*matrice_tempo[0].size()+tempo_x-1]= 1;
                pred[(tempo_y)*matrice_tempo[0].size()+tempo_x-1]=tempolin;
                file.push((tempo_y)*matrice_tempo[0].size()+tempo_x-1);
            }
            //droite
            if( (matrice_tempo[tempo_y][tempo_x+1]==0 || matrice_tempo[tempo_y][tempo_x+1]==4 || matrice_tempo[tempo_y][tempo_x+1]==6) && marquage[(tempo_y)*matrice_tempo[0].size()+tempo_x+1]==0)
            {
                //std::cout<<std::endl<<"AFFICHAGE 4 d"<<std::endl;
                marquage[(tempo_y)*matrice_tempo[0].size()+tempo_x+1]= 1;
                pred[(tempo_y)*matrice_tempo[0].size()+tempo_x+1]=tempolin;
                file.push((tempo_y)*matrice_tempo[0].size()+tempo_x+1);
            }
        }


        // on affiche le chemin de mario trouv� grace au BFS

        // on empile pour pas afficher mario a l'envers
        while (tempolin != -1)
        {
            pile.push(tempolin);
            tempolin=pred[tempolin];
        }

        int tempo_affiche=0;
        int tempo_affiche_x=0;
        int tempo_affiche_y=0;

        tempo_affiche= pile.top();
        pile.pop();
        tempo_affiche_x= tempo_affiche%matrice_tempo[0].size();
        tempo_affiche_y= tempo_affiche/matrice_tempo[0].size();
        int tempo_affiche_x_prec=tempo_affiche_x;
        int tempo_affiche_y_prec=tempo_affiche_y;
      // std::cout<<std::endl<<"tempo_affiche_x = "<<tempo_affiche_x<<"tempo_affiche_y = "<<tempo_affiche_y<<std::endl;

        // on d�pile
        while (!pile.empty())
        {

            // On efface la position pr�c�dente de mario

            if(niv.getCaseMatrice(tempo_affiche_y, tempo_affiche_x) == 4)
            {
                rectfill(m_buffer, 67+35*tempo_affiche_x, 36+35*tempo_affiche_y, 67+35*tempo_affiche_x+31, 36+33+35*tempo_affiche_y, makecol(255,255,255));
                draw_sprite(m_buffer, m_blocs[3], 66+35*tempo_affiche_x, 36+35*tempo_affiche_y);
            }
            else if(niv.getCaseMatrice(tempo_affiche_y, tempo_affiche_x) == 0)
            {
                 rectfill(m_buffer, 67+35*tempo_affiche_x, 36+35*tempo_affiche_y, 67+35*tempo_affiche_x+31, 36+33+35*tempo_affiche_y, makecol(255,255,255));
            }
            else if(niv.getCaseMatrice(tempo_affiche_y, tempo_affiche_x) == 6)
            {
                 rectfill(m_buffer, 67+35*tempo_affiche_x, 36+35*tempo_affiche_y, 67+35*tempo_affiche_x+31, 36+33+35*tempo_affiche_y, makecol(240,180,160));
            }



            // on change la position  de mario sur le buffer
            tempo_affiche= pile.top();
            pile.pop();
            tempo_affiche_x= tempo_affiche%matrice_tempo[0].size();
            tempo_affiche_y= tempo_affiche/matrice_tempo[0].size();
           // std::cout<<std::endl<<"tempo_affiche_x = "<<tempo_affiche_x<<"tempo_affiche_y = "<<tempo_affiche_y<<std::endl;

            // on l'affiche
            if(tempo_affiche_x_prec < tempo_affiche_x)
                draw_sprite(m_buffer, m_personnage[1], 72+35* (tempo_affiche_x), 36+35*(tempo_affiche_y));
            else if(tempo_affiche_x_prec > tempo_affiche_x)
                draw_sprite(m_buffer, m_personnage[2], 72+35* (tempo_affiche_x), 36+35*(tempo_affiche_y));
            else if(tempo_affiche_y_prec < tempo_affiche_y)
                draw_sprite(m_buffer, m_personnage[0], 72+35* (tempo_affiche_x), 36+35*(tempo_affiche_y));
            else if(tempo_affiche_y_prec > tempo_affiche_y)
                draw_sprite(m_buffer, m_personnage[3], 72+35* (tempo_affiche_x), 36+35*(tempo_affiche_y));

            blit(m_buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
            rest(100);

            tempo_affiche_x_prec=tempo_affiche_x;
            tempo_affiche_y_prec=tempo_affiche_y;

        }

        /// Comme mario est en position pour pousser on d�place la caisse

        rectfill(m_buffer, 67+35*(box[noeud->get_m_index()][1]-dx), 36+35*(box[noeud->get_m_index()][0]-dy), 67+35*(box[noeud->get_m_index()][1]-dx)+31, 36+33+35*(box[noeud->get_m_index()][0]-dy), makecol(255,255,255));
        if(niv.getCaseMatrice(box[noeud->get_m_index()][0], box[noeud->get_m_index()][1]) == 4)
        draw_sprite(m_buffer, m_blocs[2], 66+35*box[noeud->get_m_index()][1], 36+35*box[noeud->get_m_index()][0]);
        else draw_sprite(m_buffer, m_blocs[1], 66+35*box[noeud->get_m_index()][1], 36+35*box[noeud->get_m_index()][0]);

        /// On affiche mario a l'ancien emplacement de la caisse (translation)

         rectfill(m_buffer, 67+35*tempo_affiche_x, 36+35*tempo_affiche_y, 67+35*tempo_affiche_x+31, 36+33+35*tempo_affiche_y, makecol(255,255,255));
          if(niv.getCaseMatrice(tempo_affiche_y, tempo_affiche_x) == 6)
         {
                 rectfill(m_buffer, 67+35*tempo_affiche_x, 36+35*tempo_affiche_y, 67+35*tempo_affiche_x+31, 36+33+35*tempo_affiche_y, makecol(240,180,160));
         }
         draw_sprite(m_buffer, m_personnage[1], 72+35* (box[noeud->get_m_index()][1]-dx), 36+35*(box[noeud->get_m_index()][0]-dy));
         mario.setPosx(box[noeud->get_m_index()][1]-dx);
         mario.setPosy((box[noeud->get_m_index()][0]-dy));

     /*    ///dessin de la matrice � l'�cran
        for(int i=0; i<=NB_CASES_LARGEUR; i++)
        {
            line(m_buffer, 30+35*(i+1), 35, 30+35*(i+1), 560, makecol(0,0,0));
        }
        for(int j=0; j<=NB_CASES_HAUTEUR; j++)
        {
            line(m_buffer, 65, 35*(j+1), 730, 35*(j+1), makecol(0,0,0));
        }*/

        blit(m_buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
        rest(100);

    }
}
