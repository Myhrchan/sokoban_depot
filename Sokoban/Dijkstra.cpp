void Maze::Dijkstra(Niveau& niv,Heros mario, Allegro& alleg)
{
    std::ofstream fic("sortie.txt", std::ios::out | std::ios::trunc);
    std::unordered_set<Noeud*, Noeud::Hash, Noeud::Equality> hashTable;
    if (fic)
    {
            std::unordered_set<Noeud*, Noeud::Hash, Noeud::Equality> hashTable;
            fic<<" ///////////// Dijkstra \\\\\\\\\\\\\ "<<std::endl;
           std::cout<<std::endl<<std::endl<<"Dijkstra ! ";
           std::priority_queue<Noeud, vector<Noeud>, Noeud::Ordo> file_prio;
           int i,j;
           int b=0;
           bool fin=false;
           std::vector<int> tempo_tab ;
           int noeud_ajoute=0;
           int noeud_pas_ajoute=0;

           std::cout<<std::endl<<"Dijkstra 1 ";
           ZAP(niv,mario.getPosx(),mario.getPosy(),niv.get_m_box());

           std::cout<<std::endl<<"Dijkstra 2 ";
           Noeud premier (niv.get_m_box(),m_zone_accessible);
           //Noeud courant (niv.get_m_box(),m_zone_accessible);
           std::cout<<std::endl<<"Dijkstra 3 ";

           file_prio.push_back(premier);

           while (file_prio.size() !=0 && fin == false  )
       {
            std::cout<<"Taille de la file avant : " << file_prio.size() <<std::endl ;
            Noeud* courant = new Noeud(file_prio.front());
            file_prio.pop();

            fic<<" zone accessibilit� : "<<std::endl;


            for (int a =0; a<niv.getTailley(); a++)
            {
                for (int b =0; b<niv.getTaillex(); b++)
                {
                    fic<<courant->getAccessible(a,b);
                }
                fic<<std::endl;
            }
            fic<<std::endl<<std::endl;


            for (int c=0; c<niv.getTailley(); c++)
            {
                for (int d=0; d<niv.getTaillex(); d++)
                {
                    fic<<niv.getCaseMatrice(c,d);
                }
                fic<<std::endl;
            }

            fic<<std::endl<<std::endl;

            for (int e =0; e< niv.getNbBox(); e++)
            {
                fic<<"position de la caisse "<< e << " en y : "<<(courant->getBox(e))[0]<<"  en x :"<<(courant->getBox(e))[1]<<std::endl;
            }

            fic<<"--------------------------------------------------------------------------------------------------"<<std::endl;

            if (courant->noeud_solution(niv)) // si courant est une solution ( a completer avec le sous programme necessaire )
            {
                std::cout<<"FINI";
                affichage_BFS_DFS(courant, alleg, niv);
                fin= true; // le jeu est fini
            }
            else
            {
                for(i=0; i<niv.getNbBox(); i++)
                {
                    for (j=0; j<4; j++)
                    {
                        if ( PoussageCaisse(*courant,niv,j,i) == true )
                        {
                            //std::cout<<std::endl<<"Dijkstra 4 !!! ";
                            tempo_tab=courant->getBox(i);
                            //std::cout<<std::endl<<" 1 : " <<courant<<std::endl;
                            Noeud nouveau (courant->get_m_box(),m_zone_accessible,courant,j,i,(courant->m_poid)+1);
                            //std::cout<<std::endl<<" 2 : " <<nouveau.get_Parent()<<std::endl;
                            ZAP(niv,tempo_tab[1],tempo_tab[0],nouveau.get_m_box()); // calcul de la nouvelle ZAP
                            nouveau.setm_accessible(m_zone_accessible); // on set la zone accessible de la nouvelle tab en fonction des position des caisses

                            //std::cout<<"nouvelle position de la caisse 0 en y : "<<(nouveau.getBox(i))[0]<<"en x :"<<(nouveau.getBox(i))[1]<<std::endl;
                            //on regarde si il est pas dans la table de hachage et on l'ajoute
                            //si il est pas dans la table de hachage a la file


/// la on rajoute une verification que le poids du Noeud soit inf�rieur au poids du noeud similaire dans la table de Hash
                            auto add = hashTable.insert(&nouveau);
                            noeud_ajoute = noeud_ajoute+add.second;
                            noeud_pas_ajoute = noeud_pas_ajoute++;
                            if (add.second==true)
                            {
                                std::cout<<" AJOUT A LA TABLE "<<std::endl;
                                file_prio.push(nouveau); //Nouveau noeud : on l'ajoute � la file
                            }
                            b++;


                        }
                    }
                }
            }
       }
    if ( fin == false ) "aucune solution";
    std::cout<<std::endl<<"Noeud_ajoute = "<< noeud_ajoute;
    std::cout<<"   Noeud_Pas_ajoute = "<< noeud_pas_ajoute<<std::endl;
    fic.close();
    }

}
