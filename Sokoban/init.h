#ifndef INIT_H_INCLUDED
#define INIT_H_INCLUDED
#include "header.h"
#include "Noeud.h"
#include "Heros.h"

class Allegro
{
private:
    std::vector<BITMAP*> m_blocs; /// contient les images des murs, blocs et goals
    std::vector<BITMAP*> m_personnage; /// contient les images du personnage
    BITMAP* m_buffer;
public:
    Allegro();
    ~Allegro();

    void allegroInit();
    BITMAP* getBitmapPersonnage(int indice); /// r�cup�re le bitmap demand� dans le vecteur m_personnage
    BITMAP* getBitmapBloc(int indice); /// r�cup�re le bitmap demand� dans le vecteur m_bloc
    BITMAP* chargerImage(std::string nomFichierImage); /// renvoie le bitmap* d'une image
    void loadAllImages(); /// chargement en m�moire de la totalit� des images
    std::string choixNiveau();
    int choixMode(bool* detection_deadlock);
    void affichageMatrice(std::vector<std::vector<int> > matrice, Heros& mario, Niveau& niv, int fin); /// affichage de la totalit� de la matrice � l'�cran
    void affichage_Algo(Noeud* noeud, Niveau& niv, Heros& mario);
};

#endif // INIT_H_INCLUDED

