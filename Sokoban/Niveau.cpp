#include "Niveau.h"

Niveau::Niveau()
       : m_chekpoints_valide(0), m_resolu(false), nb_goals(0), m_taillex(NB_CASES_LARGEUR), m_tailley(NB_CASES_HAUTEUR)
{
    int i;
    m_matrice.resize(NB_CASES_HAUTEUR);
    for(i=0; i<NB_CASES_HAUTEUR; i++)
    {
        m_matrice[i].resize(NB_CASES_LARGEUR);
    }

    ///Tableau de marquage par d�faut
    m_tableau_marquage.resize(getTailley());
    for(int i=0; i<getTailley(); i++)
    {
        m_tableau_marquage[i].resize(m_taillex);
    }
    // on initialise le tableau de marquage a 0
    for (int i=0;i<getTailley(); i++)
    {
        for (int j=0;j<getTaillex();j++)
        {
            setMarquage(i,j,0);
        }
    }
}

Niveau::~Niveau()
{
}

bool Niveau::getResolu()
{
    return m_resolu;
}

void Niveau::setResolu(bool etat)
{
    m_resolu = etat;
}

void Niveau::estResolu()
{
    if(m_chekpoints_valide == nb_goals)
    {
        m_resolu = true;
    }
}

int Niveau::getNbGoals()
{
    return nb_goals;
}

int Niveau::getNbBox()
{
    return nb_box;
}

int* Niveau::getBox(unsigned int indice)
{
    int* tempotab=new int[2];
    if(indice < m_pos_box.size() && indice >= 0)
    {
         tempotab [0] = m_pos_box[indice][0];
         tempotab [1] = m_pos_box[indice][1];
         return tempotab;
    }
    return NULL;

}

std::vector <std::vector <int> >  Niveau::get_m_box()
{
    return m_pos_box;
}

void Niveau::reinitGoals()
{
    for(int i=0; i<nb_goals; i++)
    {
        m_goals[i].resize(0);
    }
    m_goals.resize(0);
    for(unsigned int i=0; i<m_pos_box.size(); i++)
    {
        m_pos_box[i].resize(0);
    }
    m_pos_box.resize(0);
}

void Niveau::reinitTab()
{
    int i, j;
    m_matrice.resize(NB_CASES_HAUTEUR);
    for(i=0; i<NB_CASES_HAUTEUR; i++)
    {
        m_matrice[i].resize(NB_CASES_LARGEUR);
    }
    for(i=0; i<NB_CASES_HAUTEUR; i++)
        for(j=0; j<NB_CASES_LARGEUR; j++)
            m_matrice[i][j]=0;
}

/// R�initialise ou augmente le nombre de checkpoints
void Niveau::setCheckpoint(signed int i)
{
    if(i==0) m_chekpoints_valide = 0;
    if(i==1 || i==-1) m_chekpoints_valide =  m_chekpoints_valide+i;
}

int Niveau::getCheckpoint()
{
    return m_chekpoints_valide;
}

std::vector<std::vector<int> > Niveau::getGoals()
{
    return m_goals;
}

/// Retourne une r�f�rence sur la matrice (pour affichage)
std::vector<std::vector<int> > Niveau::getMatrice()
{
    std::vector<std::vector<int> > mat = m_matrice;
    return mat;
}

int Niveau::getCaseMatrice(int haut, int larg)
{
    if(haut < m_tailley && haut >= 0 && larg < m_taillex && larg >= 0)
        return m_matrice[haut][larg];
    return 0;
}

int Niveau::getMarquage(int x, int y)
{
    if(x < m_tailley && x >= 0 && y < m_taillex && y >= 0)
        return m_tableau_marquage[x][y];
    return 0;
}

void Niveau::setTaillex(signed int var)
{
    if(var > 1 || var < -1) m_taillex = var;
    else m_taillex = m_taillex + var;
}

void Niveau::setTailley(signed int var)
{
    if(var > 1 || var < -1) m_tailley = var;
    else m_tailley = m_tailley + var;
}

int Niveau::getTaillex()
{
    return m_taillex;
}

int Niveau::getTailley()
{
    return m_tailley;
}

/// Modifie le contenu d'une case de la matrice de coordonn�es x, y
void Niveau::setCaseMatrice(int x, int y, int numero)
{
    if(numero >=0  && numero <=6)
        m_matrice[x][y] = numero;
}

/// Modifie le contenu d'une case de la matrice de coordonn�es x, y
void Niveau::setMarquage(int x, int y, int numero)
{
    if(numero == 1) m_tableau_marquage[x][y]=m_tableau_marquage[x][y]+numero;
    else if (numero== 0) m_tableau_marquage[x][y]=0;

}

void Niveau::afficherMarquage()
{
    int i,j;
    for (i=0;i<m_tailley; i++)
    {
        for (j=0;j<m_taillex;j++)
        {
            std::cout<<getMarquage(i,j);
        }
        std::cout<<std::endl;
    }
}

void Niveau::afficherMatrice()
{
    int i,j;
    for (i=0;i<m_tailley; i++)
    {
        for (j=0;j<m_taillex;j++)
        {
            std::cout<<getCaseMatrice(i,j);
        }
        std::cout<<std::endl;
    }
}

/// Cherche le nombre de checkpoint valid�s dans la matrice
void Niveau::indtcp()
{
    setCheckpoint(0);
    for(int i=0; i<getNbGoals(); i++)
    {
        if(m_matrice[m_goals[i][0]][m_goals[i][1]] == 2 || m_matrice[m_goals[i][0]][m_goals[i][1]] == 3)
            setCheckpoint(1);
    }
    estResolu();
}

///Modifie la taille de la matrice en fonction de ce quelle contient
///A appeler apr�s le chargement
void Niveau::reductionMatrice()
{
    bool cond = true; /// condition qui v�rifie s'il n'y a que des 6 sur la ligne ou la colonne
    //cond = true s'il n'y a que des 6
    //cond = false s'il y a autre chose

    ///V�rif des colonnes
    for(int i=m_taillex; i>1; i--)
    {
        for(int temp=0; temp<m_tailley-1; temp++)
        {
            if(m_matrice[temp][i] != 6) cond = false;
        }
        if (cond == true) setTaillex(-1);
        cond = true;
    }

    ///V�rif des lignes
    for(int j=m_tailley-1; j>0; j--)
    {
        for(int temp=0; temp<m_taillex-1; temp++)
        {
            if(m_matrice[j][temp] != 6) cond = false;
        }
        if (cond == true) setTailley(-1);
        cond = true;
    }

    ///On modifie la taille de la matrice
    m_matrice.resize(getTailley());
    for(int i=0; i<getTailley(); i++)
    {
        m_matrice[i].resize(getTaillex());
    }

    ///On g�re la taille du tableau de marquage en fonction de la taille de la matrice
    m_tableau_marquage.resize(getTailley());
    for(int i=0; i<getTailley(); i++)
    {
        m_tableau_marquage[i].resize(getTaillex());
    }
    // on initialise le tableau de marquage a 0
    for (int i=0;i<getTailley(); i++)
    {
        for (int j=0;j<getTaillex();j++)
        {
            setMarquage(i,j,0);
        }
    }
}

/// Chargement de la matrice en fonction des donn�es r�cup�r�es dans le fichier txt
void Niveau::chargementNiveau(int* posx, int* posy, std::string nom_niveau, int mode)
{
    std::string nomFichier = "levels/";
    nomFichier+=nom_niveau;
    std::ifstream fic(nomFichier.c_str(), std::ios::in);
    int temp = 0;
    nb_goals = 0;
    nb_box = 0;
    std::vector<int> goal_temp;
    std::vector<int> box_temp;
    box_temp.resize(2);
    goal_temp.resize(2);
    if(fic)
    {
        for(int i=0; i<NB_CASES_HAUTEUR; i++)
        {
            for(int j=0; j<NB_CASES_LARGEUR; j++)
            {
                /// On r�cup�re le num�ro
                fic >> temp;
                ///si c'est une 0 (sol) ou 1 (mur) on remplit la matrice
                if(temp!=4 && temp!=5 && temp!=2)
                    setCaseMatrice(i, j, temp);
                ///s'il s'agit d'un 2 (bloc poussable) et qu'on ne fait pas un BFS on remplit la matrice
                if(temp == 2 && mode != 3 && mode !=4 && mode!=5)
                    setCaseMatrice(i, j, temp);
                /// si on a un bloc, on enregistre ses coordonn�es
                if(temp==2 || temp == 3)
                {
                    box_temp[0] = i;
                    box_temp[1] = j;
                    m_pos_box.push_back(box_temp);
                    nb_box++;
                }
                if(temp==5)
                {
                    *posx = j;
                    *posy = i;
                }
                /// si on a un objectif ou une case plac�e, on enregistre la position du checkpoint en m�moire
                if(temp==4 || temp == 3)
                {
                    goal_temp[0] = i;
                    goal_temp[1] = j;
                    m_goals.push_back(goal_temp);
                    nb_goals++;
                    if (mode== 3 || mode ==4 || mode==5 )   setCaseMatrice(i, j, 4);
                    //else setCaseMatrice(i, j, temp);
                }
            }
        }
        setCheckpoint(0);
        fic.close();
        reductionMatrice();
    }
    else
        std::cout << "Impossible de charger le fichier " << nomFichier << std::endl;
}
