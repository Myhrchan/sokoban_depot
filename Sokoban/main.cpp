#include "header.h"
#include "init.h"
#include "Partie.h"


int main()
{
    Allegro temp;
    Allegro& alleg = temp;
    Partie partie;
    temp.allegroInit();
    temp.loadAllImages();

    partie.boucleJeu(alleg);
    return 0;
}
END_OF_MAIN();
