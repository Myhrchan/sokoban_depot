#ifndef __Mario_V_1_0__Heros__
#define __Mario_V_1_0__Heros__

#include "header.h"
#include "Niveau.h"

class Heros
{
    private :

        //Attributs
        int m_x;//position
        int m_y;
        signed int m_vx;//vitesse
        signed int m_vy;
        bool m_deplacement; // boolean qui nous indique si mario s'est déplacé ( utile pour les algo)

    public :

        //Constructors and destructors
        Heros();
        Heros(int _y, int _direction);
        ~Heros();

        //position
        void setPosx(int posxvariation);
        void setPosy(int posyvariation);
        int getPosx() const;
        int getPosy() const;

        //vitesse
        int getVx() const;
        int getVy() const;
        void setVx(signed int nouveauVX);
        void setVy(signed int nouveauVY);

        // dépalcement
        bool getDeplacement() const;
        void setDeplacement(bool nouveau_deplacement);
        //Methodes
        void diriger_Mario(int direction);
        void varPos(int taillex, int tailley);
        void resetvar();
        void collisionsMario(Niveau& niv);


};

#endif /* defined(__Mario_V_1_0__Heros__) */
