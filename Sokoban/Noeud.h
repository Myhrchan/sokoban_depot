#ifndef NOEUD_H_INCLUDED
#define NOEUD_H_INCLUDED

#include "Niveau.h"
#include "header.h"

class Noeud
{
private :
    Noeud* m_Parent; // pointeur parent
    // tableau 2 dimensions des positions de blocks selon les indices
    std::vector <std::vector <int> >  m_pos_box;
    int m_direction; // direction block pouss�
    int m_index; // index case pouss�e
    int m_poid;
    int m_position_normalisee;
    int m_profondeur; //profondeur du noeud dans l'arbre

    // position normalis�e Mario
    std::vector<std::vector<int> > m_zone_accessible;

public :

    std::vector<std::vector <int> > getm_zone_accessible();


    struct Hash
    {
        size_t operator()(const Noeud* a) const
        {
            return (size_t) (a->m_position_normalisee + a->m_pos_box[0][0] + (a->m_pos_box[0][1])*100);
        }
    };

    struct Egual
    {

        bool operator()(const Noeud* a, const Noeud* b) const
        {

            std::vector<int> vl1;
            std::vector<int> vl2;
            int posa=0;
            int posb=0;
            for (unsigned int i =0; i<a->m_pos_box.size(); i++)
            {

                vl1.push_back(a->m_pos_box[i][0]+a->m_pos_box[i][1]*200);
            }

            std::sort(vl1.begin(),vl1.end());

             for (unsigned int i =0; i<b->m_pos_box.size(); i++)
            {

                vl2.push_back(b->m_pos_box[i][0]+b->m_pos_box[i][1]*200);
            }

            std::sort(vl2.begin(),vl2.end());

            posa = a->m_position_normalisee;
            posb = b->m_position_normalisee;
            return (posa == posb && vl1==vl2 );
        }
    };


    struct Ordo
    {
        bool operator()(const Noeud* a, const Noeud* b) const
        {
            return a->m_poid < b->m_poid ;
        }
    };

    struct Ordo_Astar
    {
        bool operator()(const Noeud* a, const Noeud* b) const
        {
            return (a->m_poid + a->m_profondeur) < (b->m_poid + b->m_profondeur) ;
        }
    };


    // constructeurs et destructeurs
    Noeud(const std::vector <std::vector <int> > &  ,std::vector<std::vector<int> > n_zone_accessible);
    Noeud(const std::vector <std::vector <int> > &  ,std::vector<std::vector<int> > n_zone_accessible,Noeud* n_Parent, int n_direction,int n_index);
    Noeud(const std::vector <std::vector <int> > & vect,std::vector<std::vector<int> > n_zone_accessible,Noeud* n_Parent, int n_direction,int n_index,Niveau& niv);
    Noeud( Noeud const& NoeudACopier);
    ~Noeud();


    Noeud& operator=(Noeud const& NoeudACopier);

    // get position caisse

    std::vector<int> getBox(unsigned int indice);
    int get_m_direction();
    std::vector <std::vector <int> >  get_m_box();
    Noeud* get_Parent();

    int get_m_poid();
    int get_m_index();
    void set_m_poid(int i);
    void set_m_profondeur(int i);
    int get_m_profondeur();

    //setteur
    void setm_accessible(std::vector<std::vector <int> > n_vect);
    void set_m_position_normalisee(int valeur);
    int get_m_position_normalisee();

    // utile au BFS
    bool noeud_solution(Niveau& niv);
    bool getAccessible(int h, int l);
    bool is_box(int y,int x, Niveau& niv);
    int calcul_heuristique( Niveau& niv);

    //bool noeud_solution(Niveau niv); // sous programme qui nous indique si le noeud si le noeud est solution
} ;



#endif // NOEUD_H_INCLUDED
