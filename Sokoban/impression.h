struct Hash
    {
        size_t operator()(const Noeud* a) const {
            return (size_t) (a->m_index_caisse_pousse+a->m_dir_caisse_pousse/*+a->m_parent*/);
        }
    };
    struct Equality {
        bool operator()(const Noeud* a, const Noeud* b) const {
        std::vector<unsigned short>  temp1 = a->m_pos_boxes;
        std::vector<unsigned short> temp2 = b->m_pos_boxes;
          //  std::sort (nv_noeud->m_pos_boxes.begin(), nv_noeud->m_pos_boxes.end());
        std::sort(temp1.begin(),temp1.end());
        std::sort(temp2.begin(),temp2.end());
    return(a->m_pos_normalise_mario == b->m_pos_normalise_mario && temp1 == temp2);
        }
    };
