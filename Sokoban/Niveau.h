#ifndef NIVEAU_H_INCLUDED
#define NIVEAU_H_INCLUDED
#include "header.h"


class Niveau
{
private:
    std::vector<std::vector<int> > m_matrice;
    std::vector<std::vector<int> > m_tableau_marquage;
    int m_chekpoints_valide;
    bool m_resolu;
    std::vector<std::vector <int> > m_goals; /// tableau des coordonn�es des goals
    std::vector <std::vector <int> >  m_pos_box; /// tableau des coordonn�es des boites
    int nb_goals;
    int nb_box;
    int m_taillex, m_tailley; ///taille de la matrice

public:
    Niveau();
    ~Niveau();

    bool getResolu();
    void setResolu(bool etat);
    void estResolu();

    std::vector<std::vector<int> > getGoals();
    int getNbGoals();
    int getNbBox();
    int* getBox(unsigned int indice);
    void reinitGoals();
    std::vector<std::vector<int> > get_m_box();

    /// Checkpoints
    void setCheckpoint(signed int i);
    int getCheckpoint(); ///retourne le nombre de checkpoints restant
    void indtcp(); /// cherche le nombre de checkpoint valid�s dans la matrice

    /// Matrice
    std::vector<std::vector<int> > getMatrice();
    void setCaseMatrice(int x, int y, int numero);
    int getCaseMatrice(int haut, int larg);
    void chargementNiveau(int* posx, int* posy, std::string nom_niveau, int mode);
    void reinitTab();
    void afficherMatrice();

    ///Tableau de Marquage
    int getMarquage(int x, int y);
    void setMarquage(int x, int y, int numero);
    void afficherMarquage();

    ///Taille matrice
    void setTaillex(signed int var);
    void setTailley(signed int var);
    int getTaillex();
    int getTailley();
    void reductionMatrice();



};

#endif // NIVEAU_H_INCLUDED
