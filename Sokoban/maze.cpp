#include "maze.h"

Maze::Maze()
{
}

Maze::~Maze()
{
}

///Retourne true si la case de [h][l] est accessible par le joueur
bool Maze::getAccessible(int h, int l)
{

    if(m_zone_accessible[h][l] == 1) return true;
    return false;
}

void Maze::setAccessible(int i, int haut, int larg)
{
    if(i == 0 || i == 1) m_zone_accessible[haut][larg] = i;
}

///Initialise la taille de la matrice zone_accessible avant utilisation
void Maze::initTailleAccessible(int hauteur, int largeur)
{
    m_zone_accessible.resize(hauteur);
    for(int i=0; i<hauteur; i++)
    {
        m_zone_accessible[i].resize(largeur);
    }
}


/// Cherche la Zone Accessible Player
int Maze::ZAP(Niveau& niv,int mario_x,int mario_y, std::vector <std::vector <int> > PosBox)
{
    std::queue<int*> file;
    int* coord = new int[2];
    int* temp = NULL;
    int* verif = new int[2];
    short int cond = 0;
    int valeur=0;

    //std::cout<<std::endl<<"ZAP 1 ";

    initTailleAccessible(niv.getTailley(),niv.getTaillex());
    ///On initialise la matrice � 0
    for(unsigned short int i=0; i<m_zone_accessible.size(); i++)
        for(unsigned short int j=0; j<m_zone_accessible[i].size(); j++)
            setAccessible(0, i, j);

    ///On enfile les coordonn�es de mario
    coord[0] = mario_x;
    coord[1] = mario_y;
    setAccessible(1, mario_y, mario_x);
    file.push(coord);

    //std::cout<<std::endl<<"ZAP 2 ";
    while(!file.empty())
    {
        temp = file.front();
        ///vers le haut, si la case d'origine contient un 0 ou un 6
        if(temp[1] > 0 && !getAccessible(temp[1]-1, temp[0])
                && (niv.getCaseMatrice(temp[1]-1, temp[0]) == 0 || niv.getCaseMatrice(temp[1]-1, temp[0]) == 6 || niv.getCaseMatrice(temp[1]-1, temp[0]) == 4))
        {
            for(int i=0; i<niv.getNbBox(); i++)
            {
                verif[0] = PosBox[i][0];
                verif[1] = PosBox[i][1];

                if(temp[0] == verif[1] && temp[1]-1 == verif[0]) cond = 1;
            }
            if (cond == 0)
            {
                setAccessible(1, temp[1]-1, temp[0]);
                coord = new int[2]();
                coord[0] = temp[0];
                coord[1] = temp[1]-1;
                file.push(coord);
            }
            else cond=0;
        }
        ///vers le bas, si la case d'origine contient un 0 ou un 6
        if(temp[1] < niv.getTailley()-1 && !getAccessible(temp[1]+1, temp[0])
                && (niv.getCaseMatrice(temp[1]+1, temp[0]) == 0 || niv.getCaseMatrice(temp[1]+1, temp[0]) == 6 || niv.getCaseMatrice(temp[1]+1, temp[0]) == 4))
        {
            for(int i=0; i<niv.getNbBox(); i++)
            {
                verif[0] = PosBox[i][0];
                verif[1] = PosBox[i][1];
                if(temp[0] == verif[1] && temp[1]+1 == verif[0]) cond = 1;
            }
            if (cond == 0)
            {
                setAccessible(1, temp[1]+1, temp[0]);
                coord = new int[2]();
                coord[0] = temp[0];
                coord[1] = temp[1]+1;
                file.push(coord);
            }
            else cond=0;
        }
        ///vers la gauche, si la case d'origine contient un 0 ou un 6
        if(temp[0] > 0 && !getAccessible(temp[1], temp[0]-1)
                && (niv.getCaseMatrice(temp[1], temp[0]-1) == 0 || niv.getCaseMatrice(temp[1], temp[0]-1) == 6 || niv.getCaseMatrice(temp[1], temp[0]-1) == 4))
        {
            for(int i=0; i<niv.getNbBox(); i++)
            {
                verif[0] = PosBox[i][0];
                verif[1] = PosBox[i][1];
                if(temp[0]-1 == verif[1] && temp[1] == verif[0]) cond = 1;
            }
            if (cond == 0)
            {
                setAccessible(1, temp[1], temp[0]-1);
                coord = new int[2]();
                coord[0] = temp[0]-1;
                coord[1] = temp[1];
                file.push(coord);
            }
            else cond=0;
        }
        ///vers la droite, si la case d'origine contient un 0 ou un 6
        if(temp[0] < niv.getTaillex()-1 && !getAccessible(temp[1], temp[0]+1)
                && (niv.getCaseMatrice(temp[1], temp[0]+1) == 0 || niv.getCaseMatrice(temp[1], temp[0]+1) == 6 || niv.getCaseMatrice(temp[1], temp[0]+1) == 4))
        {
            for(int i=0; i<niv.getNbBox(); i++)
            {
                verif[0] = PosBox[i][0];
                verif[1] = PosBox[i][1];
                if(temp[0]+1 == verif[1] && temp[1] == verif[0]) cond = 1;
            }
            if (cond == 0)
            {
                setAccessible(1, temp[1], temp[0]+1);
                coord = new int[2]();
                coord[0] = temp[0]+1;
                coord[1] = temp[1];
                file.push(coord);
            }
            else cond=0;
        }
        file.pop();
    }
    delete coord;
    delete verif;
    delete temp;


    for(int haut=0; haut<niv.getTailley(); haut++)
    {
        for(int larg=0; larg<niv.getTaillex(); larg++)
        {
            if(getAccessible(haut, larg) == true && valeur == 0)
                valeur = larg+haut*(niv.getTaillex());
        }
    }
    return valeur;
}


/*
On prend :
- la matrice vide dans niveau
- la position des blocs dans noeud
- la direction dans laquelle on pousse le bloc
- l'indice du bloc � pousser
*/
bool Maze::PoussageCaisse(Noeud noeud, Niveau& niv, int direction, int indice)
{

    std::vector<int> box = noeud.getBox(indice);
    //std::cout<<std::endl<<"direction = " << direction <<std::endl;
    //std::cout<<"box[0] = " << box[0]<<" box[1] = " << box[1]<<std::endl;
    //std::cout<<std::endl<<"noeud.getAccessible(0, 0) = " <<noeud.getAccessible(0, 0) <<std::endl;
    //std::cout<<std::endl<<"niv.getCaseMatrice( 0,0 ) = " <<niv.getCaseMatrice( 0,0 ) <<std::endl;


    ///si la direction est vers le haut
    if(direction == 0 && box[0] > 0 && noeud.getAccessible(box[0]+1, box[1]) == 1 && ( niv.getCaseMatrice( box[0]-1, box[1] ) == 0  || niv.getCaseMatrice( box[0]-1, box[1]) == 4) && noeud.is_box(box[0]-1,box[1],niv)==0)
    {
        return true;
    }
    ///si la direction est vers le bas
    else if(direction == 1 && box[0] < niv.getTailley()-1 && noeud.getAccessible(box[0]-1, box[1]) == 1 && (niv.getCaseMatrice(box[0]+1, box[1] ) == 0 || niv.getCaseMatrice(box[0]+1, box[1]) == 4 ) && noeud.is_box(box[0]+1,box[1],niv)==0)
    {
        return true;
    }
    ///si la direction est vers la gauche
    else if(direction == 2 && box[1] > 0 && noeud.getAccessible(box[0], box[1]+1) == 1 && ( niv.getCaseMatrice(box[0], box[1]-1 ) == 0 || niv.getCaseMatrice( box[0], box[1]-1  ) == 4) && noeud.is_box(box[0],box[1]-1,niv)==0)
    {
        return true;
    }
    ///si la direction est vers la droite
    else if(direction == 3 && box[1] < niv.getTaillex()-1 && noeud.getAccessible(box[0], box[1]-1) == 1 && (niv.getCaseMatrice(box[0], box[1]+1 ) == 0 || niv.getCaseMatrice( box[0], box[1]+1 ) == 4) && noeud.is_box(box[0],box[1]+1,niv)==0)
    {
        return true;
    }
    return false;
}

///renvoie true si les coordonn�es i, j envoy�es correspondent � un coin = deadlock
bool Maze::estUnCoin(int i, int j, Niveau& niv)
{
    bool dead = false;

    if(i>0 && j>0)
    {
        if(niv.getCaseMatrice(i-1, j) == 1 && niv.getCaseMatrice(i, j-1) == 1)
            dead = true;
    }
    if(i>0 && j<niv.getTaillex()-1)
    {
        if(niv.getCaseMatrice(i-1, j) == 1 && niv.getCaseMatrice(i, j+1) == 1)
            dead = true;
    }
    if(i<niv.getTailley()-1 && j<niv.getTaillex()-1)
    {
        if(niv.getCaseMatrice(i+1, j) == 1 && niv.getCaseMatrice(i, j+1) == 1)
            dead = true;
    }
    if(i<niv.getTailley()-1 && j>0)
    {
        if(niv.getCaseMatrice(i+1, j) == 1 && niv.getCaseMatrice(i, j-1) == 1)
            dead = true;
    }


    return dead;
}

void Maze::detectionDeadlocks(Niveau& niv)
{
    bool dead = false;
    bool cond = false;
    std::vector<std::vector<int> > goals = niv.getGoals();
    int k=0;

    /// parcours de la matrice pour trouver les deadlocks dans les coins
    for(int i=0; i<niv.getTailley(); i++)
    {
        for(int j=0; j<niv.getTaillex(); j++)
        {
            if(niv.getCaseMatrice(i, j) == 0) /// si ce n'est pas d�j� une deadlock
            {
                dead = estUnCoin(i, j, niv);

                if(dead == true)
                {
                    niv.setCaseMatrice(i, j, 6);
                    for(int g=0; g<niv.getNbGoals(); g++)
                    {
                        if(niv.getCaseMatrice(goals[g][0], goals[g][1]) == 6) niv.setCaseMatrice(goals[g][0], goals[g][1], 0);
                    }
                }
            }
        }
    }

    ///recherche des cases coinc�es entre deux deadlocks et un mur = deadlocks
    for(int i=0; i<niv.getTailley(); i++)
    {
        for(int j=0; j<niv.getTaillex(); j++)
        {
            if(niv.getCaseMatrice(i, j) == 6) /// si c'est une deadlock
            {
                dead = false;
                cond = false;

                if(i>0 && j>0 && j<niv.getTaillex()-1) ///deadlock de type coin haut gauche
                {
                    if(niv.getCaseMatrice(i-1, j) == 1 && niv.getCaseMatrice(i, j-1) == 1 && niv.getCaseMatrice(i, j+1) == 0)
                    {
                        ///detection en ligne bas
                        k=j;
                        while(k<niv.getTaillex()-2 && dead!=true && cond != true)
                        {
                            if(niv.getCaseMatrice(i, k+1) == 6)
                                dead = true;
                            else if(niv.getCaseMatrice(i-1, k+1) != 1 || niv.getCaseMatrice(i, k+1) != 0)
                                cond = true;
                            if(dead==false && cond == false) k++;
                        }
                        ///si on a trouv� une deadlock au bout de la ligne
                        if(dead == true)
                        {
                            ///on marque toutes les cases entre les deux deadlocks en tant que deadlocks
                            for(int temp=j+1; temp<k+1; temp++)
                            {
                                niv.setCaseMatrice(i, temp, 6);
                            }
                        }
                    }
                }

                dead = false;
                cond = false;

                if(i>0 && i<niv.getTailley()-1 && j<niv.getTaillex()-1) ///deadlock de type coin haut droite
                {
                    if(niv.getCaseMatrice(i-1, j) == 1 && niv.getCaseMatrice(i, j+1) == 1 && niv.getCaseMatrice(i+1, j) == 0)
                    {
                        ///detection en colonne gauche
                        k=i;
                        while(k<niv.getTailley()-2 && dead!=true && cond != true)
                        {
                            if(niv.getCaseMatrice(k+1, j) == 6)
                                dead = true;
                            else if(niv.getCaseMatrice(k+1, j+1) != 1 || niv.getCaseMatrice(k+1, j) != 0)
                                cond = true;
                            if(dead==false && cond == false) k++;
                        }
                        ///si on a trouv� une deadlock au bout de la ligne
                        if(dead == true)
                        {
                            ///on marque toutes les cases entre les deux deadlocks en tant que deadlocks
                            for(int temp=i+1; temp<k+1; temp++)
                            {
                                niv.setCaseMatrice(temp, j, 6);
                            }
                        }
                    }
                }


                dead = false;
                cond = false;

                if(i>0 && i<niv.getTailley()-1 && j>0) ///deadlock de type coin haut gauche
                {
                    if(niv.getCaseMatrice(i-1, j) == 1 && niv.getCaseMatrice(i, j-1) == 1 && niv.getCaseMatrice(i+1, j) == 0)
                    {
                        ///detection en colonne droite
                        k=i;
                        while(k<niv.getTailley()-2 && dead!=true && cond != true)
                        {
                            if(niv.getCaseMatrice(k+1, j) == 6)
                                dead = true;
                            else if(niv.getCaseMatrice(k+1, j-1) != 1 || niv.getCaseMatrice(k+1, j) != 0)
                                cond = true;
                            if(dead==false && cond == false) k++;
                        }
                        ///si on a trouv� une deadlock au bout de la ligne
                        if(dead == true)
                        {
                            ///on marque toutes les cases entre les deux deadlocks en tant que deadlocks
                            for(int temp=i+1; temp<k+1; temp++)
                            {
                                niv.setCaseMatrice(temp, j, 6);
                            }
                        }
                    }
                }

                dead = false;
                cond = false;

                if(i<niv.getTailley()-1 && j>0 && j<niv.getTaillex()-1) ///deadlock de type coin bas gauche
                {
                    if(niv.getCaseMatrice(i+1, j) == 1 && niv.getCaseMatrice(i, j-1) == 1 && niv.getCaseMatrice(i, j+1) == 0)
                    {
                        ///detection en ligne haut
                        k=j;
                        while(k<niv.getTaillex()-2 && dead!=true && cond != true)
                        {
                            if(niv.getCaseMatrice(i, k+1) == 6)
                                dead = true;
                            else if(niv.getCaseMatrice(i+1, k+1) != 1 || niv.getCaseMatrice(i, k+1) != 0)
                                cond = true;
                            if(dead==false && cond == false) k++;
                        }
                        ///si on a trouv� une deadlock au bout de la ligne
                        if(dead == true)
                        {
                            ///on marque toutes les cases entre les deux deadlocks en tant que deadlocks
                            for(int temp=j+1; temp<k+1; temp++)
                            {
                                niv.setCaseMatrice(i, temp, 6);
                            }
                        }
                    }
                }
            }
        }
    }

    for(int g=0; g<niv.getNbGoals(); g++)
    {
        if(niv.getCaseMatrice(goals[g][0], goals[g][1]) == 6) niv.setCaseMatrice(goals[g][0], goals[g][1], 0);
    }
}


bool Maze::recherche_naif(Heros& Mario,Niveau& niv)
{

    int i;
    //int d;
    bool test =false ; // permet d'arreter la boucle
    construction_tab(Mario,niv);
    //std::cout << "TEST 2 : " << m_tab[1][0] << "   " << m_tab[1][1];
    tri_tab();
    affichage_tab();

    for (i=0; (i<4 && test == false ); i++) // tant qu'on a pas trouv�e une case accessible et non marqu�e on continue la boucle
    {

        Mario.diriger_Mario(m_tab[i][0]); // on va essayer de diriger mario
        Mario.collisionsMario(niv); // on va tester les collision ( penser aussi a tester les Deadlocks)
        if (Mario.getDeplacement()==true)
        {
            test = true ; // si mario s'est d�plac� on arrete la boucle
            niv.setMarquage(Mario.getPosy(),Mario.getPosx(),1); // on marque la position ou est mario
        }
    }
   // affichage_marquage(niv);
    Mario.setDeplacement(false);
    if (test == true) return(true);
    else return(false);
}

void Maze::tri_tab() // permet d'avoir un tableau tri� dans l'ordre croissant du nombre de marquage
{
    int maxi, i;
    int longueur =4;
    int tempo1,tempo2;

    while(longueur>0)
    {
        //on recherche la plus grande valeur du tableau non encore trie
        maxi=0;

        for(i=1; i<longueur; i++)
        {
            //std::cout<<"Test  " << i;
            if(m_tab[i][1]>m_tab[maxi][1]) maxi=i;

        }
        //on echange le plus grand element avec le dernier

        tempo1 = m_tab[maxi][0];
        tempo2 = m_tab[maxi][1];
        m_tab[maxi][0] = m_tab[(longueur-1)][0];
        m_tab[maxi][1] = m_tab[(longueur-1)][1];
        m_tab[(longueur-1)][0] = tempo1;
        m_tab[(longueur-1)][1] = tempo2;



        //on traite le reste du tableau
        longueur--;
    }
}

void Maze::construction_tab(Heros& Mario,Niveau& niv)
{
    int i,j;

    // on alloue l'ensemble de tab et on l'initalise a 0
    for (i=0; i<4; i++)
    {
        m_tab.push_back(std::vector<int>()); // on rajoute une ligne
        for ( j=0; j<2; j++)
        {
            m_tab[i].push_back(0);
        }
    }


    m_tab[0][0] = 0; // la direction
    m_tab[0][1] =  niv.getMarquage(Mario.getPosy()-1, Mario.getPosx()); // le marquage de la case destination


    m_tab[1][0] = 1; // la direction
    m_tab[1][1] =  niv.getMarquage(Mario.getPosy()+1, Mario.getPosx()); // le marquage de la case destination

    m_tab[2][0] = 2;//; // la direction
    m_tab[2][1] = niv.getMarquage(Mario.getPosy(), Mario.getPosx()-1); // le marquage de la case destination

    m_tab[3][0] = 3;//; // la direction
    m_tab[3][1] = niv.getMarquage(Mario.getPosy(), Mario.getPosx()+1); // le marquage de la case destination

}

void Maze::affichage_tab()
{
    int i,j;
    for (i=0; i<4; i++)
    {
        for ( j=0; j<2; j++)
        {
            std::cout<<m_tab[i][j];
        }
        std::cout<<std::endl;

    }
}

void Maze::affichage_marquage(Niveau& niv)
{
    int i,j;
    for (i=0; i<niv.getTailley(); i++)
    {
        for ( j=0; j<niv.getTaillex(); j++)
        {
            std::cout<<niv.getMarquage(i,j);
        }
        std::cout<<std::endl;

    }
}


void Maze::BFS(Niveau& niv,Heros mario, Allegro& alleg)
{

           std::unordered_set<Noeud*, Noeud::Hash, Noeud::Egual> hashTable;
           std::queue <Noeud*> fifo;
           bool fin=false;
           std::vector<int> tempo_tab ;
           unsigned long int Nombre_de_noeuds_visites_au_total = 1;
           unsigned long int Noeuds_explores = 0;
           unsigned long int Noeuds_encore_dans_la_fifo = 0;
           unsigned long int Noeuds_dupliques = 0;
           unsigned long int Noeuds_ajoutes = 0;
           int temp;

           // on va creer un premier noeud � partir de la disposition de base du niveau et stocker un poiteur sur ce noeud dans la file et la table de hash
           temp = ZAP(niv,mario.getPosx(),mario.getPosy(),niv.get_m_box());
           Noeud* premier = new Noeud(niv.get_m_box(),m_zone_accessible);
           premier->set_m_position_normalisee(temp);
           //auto add = hashTable.insert(premier);
           //assert(add.second==true );
                fifo.push(premier); //Nouveau noeud : on l'ajoute � la file
                Noeuds_ajoutes++;
           // on cree un pointeur sur Noeud courant qui va nous permettre de recuperer les Noeud de la file

        Noeud* courant =NULL;

           while (fifo.size() !=0 && fin == false )
       {
            courant= fifo.front();
            Noeuds_explores++;
            fifo.pop();
            if (courant->noeud_solution(niv)) // si courant est une solution ( a completer avec le sous programme necessaire )
            {
                Noeuds_encore_dans_la_fifo= fifo.size();
                std::cout<< " Noeuds_explores : " << Noeuds_explores << " Noeuds_encore_dans_la_fifo : "<<Noeuds_encore_dans_la_fifo<< " Noeuds_dupliques : "<<std::endl<<Noeuds_dupliques<<" Noeuds ajoutes = "<<Noeuds_ajoutes<<std::endl;
                std::cout<< " Nombre_de_noeuds_visites_au_total : " << Nombre_de_noeuds_visites_au_total << " Verification : "<<Noeuds_explores+Noeuds_dupliques+Noeuds_encore_dans_la_fifo <<std::endl;
                affichage_BFS_DFS_Dijkstra(courant, alleg, niv, mario);
                fin= true; // le jeu est fini
            }
            else
            {
                for(int i=0; i<niv.getNbBox(); i++)
                {
                    for (int j=0; j<4; j++)
                    {
                        if ( PoussageCaisse(*courant,niv,j,i) == true )
                        {
                            Nombre_de_noeuds_visites_au_total++;
                            tempo_tab=courant->getBox(i); // on recup�re la position du joeur apr�s avoir pousse la caisse pour calculer la nouvelle ZAP

                            Noeud* nouveau= new Noeud(courant->get_m_box(),m_zone_accessible,courant,j,i);
                            temp = ZAP(niv,tempo_tab[1],tempo_tab[0],nouveau->get_m_box()); // calcul de la nouvelle ZAP
                            nouveau->setm_accessible(m_zone_accessible); // on set la zone accessible de la nouvelle tab en fonction des position des caisse
                            nouveau->set_m_position_normalisee(temp);

                            auto add = hashTable.insert(nouveau);

                            if (add.second)
                            {
                                fifo.push(nouveau); //Nouveau noeud : on l'ajoute � la file
                                Noeuds_ajoutes++;
                            }
                            // si c'est un soublon le noeud nous sera pas utile, on le lib�re donc
                            else
                            {
                                Noeuds_dupliques++;
                                delete nouveau;
                            }
                        }
                    }
                }
            }
      }
    std::cout<<"taille table "<<hashTable.size();
      // on lib�re tous es noeud qu'on a alloue dynamoiquement pour ce faire on parcours la table de hash et free
      for (auto i = hashTable.begin(); i != hashTable.end(); ++i)
      {
         // delete *i;
      }

      std::cout<<"fin bfs";
}





void Maze::DFS(Niveau& niv,Heros mario, Allegro& alleg)
{

           std::unordered_set<Noeud*, Noeud::Hash, Noeud::Egual> hashTable;
           std::stack <Noeud*> lifo;
           bool fin=false;
           std::vector<int> tempo_tab ;
           unsigned long int Nombre_de_noeuds_visites_au_total = 1;
           unsigned long int Noeuds_explores = 0;
           unsigned long int Noeuds_encore_dans_la_lifo = 0;
           unsigned long int Noeuds_dupliques = 0;
           unsigned long int Noeuds_ajoutes = 0;
           int temp;

           // on va creer un premier noeud � partir de la disposition de base du niveau et stocker un poiteur sur ce noeud dans la file et la table de hash
           temp = ZAP(niv,mario.getPosx(),mario.getPosy(),niv.get_m_box());
           Noeud* premier = new Noeud(niv.get_m_box(),m_zone_accessible);
           premier->set_m_position_normalisee(temp);
           //auto add = hashTable.insert(premier);
           //assert(add.second==true );
                lifo.push(premier); //Nouveau noeud : on l'ajoute � la file
                Noeuds_ajoutes++;
           // on cree un pointeur sur Noeud courant qui va nous permettre de recuperer les Noeud de la file

        Noeud* courant =NULL;

           while (lifo.size() !=0 && fin == false )
       {
            courant= lifo.top();
            Noeuds_explores++;
            lifo.pop();
            if (courant->noeud_solution(niv)) // si courant est une solution ( a completer avec le sous programme necessaire )
            {
                Noeuds_encore_dans_la_lifo= lifo.size();
                std::cout<< " Noeuds_explores : " << Noeuds_explores << " Noeuds_encore_dans_la_lifo : "<<Noeuds_encore_dans_la_lifo<< " Noeuds_dupliques : "<<std::endl<<Noeuds_dupliques<<" Noeuds ajoutes = "<<Noeuds_ajoutes<<std::endl;
                std::cout<< " Nombre_de_noeuds_visites_au_total : " << Nombre_de_noeuds_visites_au_total << " Verification : "<<Noeuds_explores+Noeuds_dupliques+Noeuds_encore_dans_la_lifo <<std::endl;
                affichage_BFS_DFS_Dijkstra(courant, alleg, niv, mario);
                fin= true; // le jeu est fini
            }
            else
            {
                for(int i=0; i<niv.getNbBox(); i++)
                {
                    for (int j=0; j<4; j++)
                    {
                        if ( PoussageCaisse(*courant,niv,j,i) == true )
                        {
                            Nombre_de_noeuds_visites_au_total++;
                            tempo_tab=courant->getBox(i); // on recup�re la position du joeur apr�s avoir pousse la caisse pour calculer la nouvelle ZAP

                            Noeud* nouveau= new Noeud(courant->get_m_box(),m_zone_accessible,courant,j,i);
                            temp = ZAP(niv,tempo_tab[1],tempo_tab[0],nouveau->get_m_box()); // calcul de la nouvelle ZAP
                            nouveau->setm_accessible(m_zone_accessible); // on set la zone accessible de la nouvelle tab en fonction des position des caisse
                            nouveau->set_m_position_normalisee(temp);

                            auto add = hashTable.insert(nouveau);

                            if (add.second)
                            {
                                lifo.push(nouveau); //Nouveau noeud : on l'ajoute � la file
                                Noeuds_ajoutes++;
                            }
                            // si c'est un soublon le noeud nous sera pas utile, on le lib�re donc
                            else
                            {
                                Noeuds_dupliques++;
                                delete nouveau;
                            }
                        }
                    }
                }
            }
      }
    std::cout<<"taille table "<<hashTable.size();
      // on lib�re tous es noeud qu'on a alloue dynamoiquement pour ce faire on parcours la table de hash et free
      for (auto i = hashTable.begin(); i != hashTable.end(); ++i)
      {
         // delete *i;
      }

      std::cout<<"fin dfs";
}


void Maze::Best_First_Search(Niveau& niv,Heros mario, Allegro& alleg)
{
           std::unordered_set<Noeud*, Noeud::Hash, Noeud::Egual> hashTable;
           std::priority_queue<Noeud*, std::vector<Noeud*>, Noeud::Ordo> file_prio;
           bool fin=false;
           std::vector<int> tempo_tab ;
           unsigned long int Nombre_de_noeuds_visites_au_total = 1;
           unsigned long int Noeuds_explores = 0;
           unsigned long int Noeuds_encore_dans_la_lifo = 0;
           unsigned long int Noeuds_dupliques = 0;
           unsigned long int Noeuds_ajoutes = 0;
            int temp=0;

           // on va creer un premier noeud � partir de la disposition de base du niveau et stocker un poiteur sur ce noeud dans la file et la table de hash
           temp=ZAP(niv,mario.getPosx(),mario.getPosy(),niv.get_m_box());
           Noeud* premier = new Noeud(niv.get_m_box(),m_zone_accessible);
           premier->set_m_position_normalisee(temp);

           auto add = hashTable.insert(premier);
            file_prio.push(premier); //Nouveau noeud : on l'ajoute � la file

           // on cree un pointeur sur Noeud courant qui va nous permettre de recuperer les Noeud de la file

        Noeud* courant =NULL;

           while (file_prio.size() !=0 && fin == false  )
       {
            courant= file_prio.top();
            Noeuds_explores++;
            file_prio.pop();
            if (courant->noeud_solution(niv)) // si courant est une solution ( a completer avec le sous programme necessaire )
            {
                Noeuds_encore_dans_la_lifo= file_prio.size();
                std::cout<< " Noeuds_explores : " << Noeuds_explores << " Noeuds_encore_dans_la_lifo : "<<Noeuds_encore_dans_la_lifo<<std::endl<< " Noeuds_dupliques : "<<Noeuds_dupliques<<" Noeuds ajoutes = "<<Noeuds_ajoutes<<std::endl;
                std::cout<< " Nombre_de_noeuds_visites_au_total : " << Nombre_de_noeuds_visites_au_total << " Verification : "<<Noeuds_explores+Noeuds_dupliques+Noeuds_encore_dans_la_lifo <<std::endl;
                affichage_BFS_DFS_Dijkstra(courant, alleg, niv, mario);
                fin= true; // le jeu est fini
            }
            else
            {
                for(int i=0; i<niv.getNbBox(); i++)
                {
                    for (int j=0; j<4; j++)
                    {
                        if ( PoussageCaisse(*courant,niv,j,i) == true )
                        {
                            Nombre_de_noeuds_visites_au_total++;
                            tempo_tab=courant->getBox(i); // on recup�re la position du joeur apr�s avoir pousse la caisse pour calculer la nouvelle ZAP

                            Noeud* nouveau= new Noeud(courant->get_m_box(),m_zone_accessible,courant,j,i);
                            temp=ZAP(niv,tempo_tab[1],tempo_tab[0],nouveau->get_m_box()); // calcul de la nouvelle ZAP
                            nouveau->set_m_position_normalisee(temp);
                            nouveau->setm_accessible(m_zone_accessible); // on set la zone accessible de la nouvelle tab en fonction des position des caisse

                            auto add = hashTable.insert(nouveau);

                            if (add.second)
                            {
                                file_prio.push(nouveau); //Nouveau noeud : on l'ajoute � la file
                                Noeuds_ajoutes++;
                            }
                            // si c'est un soublon le noeud nous sera pas utile, on le lib�re donc
                            else
                            {
                                Noeuds_dupliques++;
                                delete nouveau;
                            }
                        }
                    }
                }
            }
      }
      std::cout<<"taille table "<<hashTable.size();
      // on lib�re tous es noeud qu'on a alloue dynamoiquement pour ce faire on parcours la table de hash et free
      for (auto i = hashTable.begin(); i != hashTable.end(); ++i)
      {
         // delete *i;
      }

      std::cout<<"fin best fs";
}


void Maze::Astar(Niveau& niv,Heros mario, Allegro& alleg)
{
           std::unordered_set<Noeud*, Noeud::Hash, Noeud::Egual> hashTable;
           std::priority_queue<Noeud*, std::vector<Noeud*>, Noeud::Ordo_Astar> file_prio;
           bool fin=false;
           std::vector<int> tempo_tab ;
           unsigned long int Nombre_de_noeuds_visites_au_total = 1;
           unsigned long int Noeuds_explores = 0;
           unsigned long int Noeuds_encore_dans_la_lifo = 0;
           unsigned long int Noeuds_dupliques = 0;
           unsigned long int Noeuds_ajoutes = 0;
            int temp=0;
            int profondeur =0;

           // on va creer un premier noeud � partir de la disposition de base du niveau et stocker un poiteur sur ce noeud dans la file et la table de hash
           temp=ZAP(niv,mario.getPosx(),mario.getPosy(),niv.get_m_box());
           Noeud* premier = new Noeud(niv.get_m_box(),m_zone_accessible);
           premier->set_m_position_normalisee(temp);
           premier->set_m_profondeur(profondeur);

           auto add = hashTable.insert(premier);
           file_prio.push(premier); //Nouveau noeud : on l'ajoute � la file

           // on cree un pointeur sur Noeud courant qui va nous permettre de recuperer les Noeud de la file

        Noeud* courant =NULL;

           while (file_prio.size() !=0 && fin == false  )
       {
            courant= file_prio.top();
            Noeuds_explores++;
            profondeur = courant->get_m_profondeur() +1;
            // std::cout << file_prio.size() << std::endl;
            file_prio.pop();
             if (courant->noeud_solution(niv)) // si courant est une solution ( a completer avec le sous programme necessaire )
            {
                Noeuds_encore_dans_la_lifo= file_prio.size();
                std::cout<< " Noeuds_explores : " << Noeuds_explores << " Noeuds_encore_dans_la_lifo : "<<Noeuds_encore_dans_la_lifo<< " Noeuds_dupliques : "<<Noeuds_dupliques<<" Noeuds ajoutes = "<<Noeuds_ajoutes<<std::endl;
                std::cout<< " Nombre_de_noeuds_visites_au_total : " << Nombre_de_noeuds_visites_au_total << " Verification : "<<Noeuds_explores+Noeuds_dupliques+Noeuds_encore_dans_la_lifo <<std::endl;
                affichage_BFS_DFS_Dijkstra(courant, alleg, niv, mario);
                fin= true; // le jeu est fini
            }
            else
            {
                for(int i=0; i<niv.getNbBox(); i++)
                {
                    for (int j=0; j<4; j++)
                    {
                        if ( PoussageCaisse(*courant,niv,j,i) == true )
                        {
                            Nombre_de_noeuds_visites_au_total++;
                            tempo_tab=courant->getBox(i); // on recup�re la position du joeur apr�s avoir pousse la caisse pour calculer la nouvelle ZAP

                            Noeud* nouveau= new Noeud(courant->get_m_box(),m_zone_accessible,courant,j,i);
                            temp=ZAP(niv,tempo_tab[1],tempo_tab[0],nouveau->get_m_box()); // calcul de la nouvelle ZAP
                            nouveau->set_m_position_normalisee(temp);
                            nouveau->setm_accessible(m_zone_accessible); // on set la zone accessible de la nouvelle tab en fonction des position des caisse
                            nouveau->set_m_profondeur(profondeur);

                            auto add = hashTable.insert(nouveau);

                            if (add.second)
                            {
                                file_prio.push(nouveau); //Nouveau noeud : on l'ajoute � la file
                                Noeuds_ajoutes++;
                            }
                            // si c'est un soublon le noeud nous sera pas utile, on le lib�re donc
                            else
                            {
                                Noeuds_dupliques++;
                                delete nouveau;
                            }
                        }
                    }
                }
            }
      }
      // on lib�re tous es noeud qu'on a alloue dynamoiquement pour ce faire on parcours la table de hash et free
      for (auto i = hashTable.begin(); i != hashTable.end(); ++i)
      {
       //delete *i;

      }
}

void Maze::affichage_BFS_DFS_Dijkstra(Noeud* noeud_final, Allegro& alleg, Niveau& niv, Heros mario)
{
    Noeud* tempo_noeud=noeud_final;
    int i =0;
    //std::cout<<tempo_noeud<<"   "<<(tempo_noeud->get_Parent()) << "    " << ((tempo_noeud->get_Parent())->get_Parent()) ;
    std::stack<Noeud*> pile;
    //std::cout<<"  AFFICHAGE_BFS 1";
    while (tempo_noeud != NULL)
    {
        //std::cout<< "AFFICHAGE BFS 2 :" <<tempo_noeud<<std::endl ;
        pile.push(tempo_noeud); // on aoute noeud a la pile
        tempo_noeud = tempo_noeud->get_Parent(); // on pase au noeud suivant jusqu'a avoir traite tous les noeuds
    }
    while (pile.empty()  != true ) // tant que la pile n'est pas vide
    {
        //std::cout<< "AFFICHAGE BFS 3";
        tempo_noeud =pile.top();
        pile.pop();
        //std::cout<<std::endl<<"Affichage Noeud "<<std::endl;
        alleg.affichage_Algo(tempo_noeud, niv, mario);
        delete tempo_noeud; // on lib�re le Noeud alloue
    }
}
