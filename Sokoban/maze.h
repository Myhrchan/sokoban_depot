#ifndef MAZE_H_INCLUDED
#define MAZE_H_INCLUDED
#include "header.h"
#include "Niveau.h"
#include "Heros.h"
#include "Noeud.h"
#include "init.h"
#include "header.h"

class Maze
{
private:
   std::vector<std::vector<int> > m_tab;
   std::vector<std::vector<int> > m_zone_accessible;


public:
    Maze();
    ~Maze();

    ///Deadlocks
    bool estUnCoin(int i, int j, Niveau& niv);
    void detectionDeadlocks(Niveau& niv);

    void affichage_BFS_DFS_Dijkstra(Noeud* noeud_final, Allegro& alleg, Niveau& niv, Heros mario);

    ///Action sur la matrice m_zone_accessible
    bool getAccessible(int h, int l);
    void setAccessible(int i, int haut, int larg);
    void initTailleAccessible(int hauteur, int largeur);
    int ZAP(Niveau& niv,int mario_x,int mario_y, std::vector <std::vector <int> > PosBox);

    ///BFs
    bool PoussageCaisse(Noeud noeud, Niveau& niv, int direction, int indice);
    void BFS(Niveau& niv,Heros mario, Allegro& alleg);
    void DFS(Niveau& niv,Heros mario, Allegro& alleg);

    ///Algo Force brute
    bool recherche_naif (Heros& Mario,Niveau& niv);
    void construction_tab(Heros& Mario,Niveau& niv);
    void tri_tab();
    void affichage_tab();
    void affichage_marquage(Niveau& niv);

    ///Algo Best First Search
    int calcul_heuristique(std::vector<int> caisse, Niveau& niv);
    void Best_First_Search(Niveau& niv,Heros mario, Allegro& alleg);
    void Astar(Niveau& niv,Heros mario, Allegro& alleg);


};



#endif // MAZE_H_INCLUDED
