#include "Heros.h"

//Constructeur par d�fault
Heros::Heros()
      :m_x(0), m_y(0), m_vx(0), m_vy(0)
{

}

//Destructeur
Heros::~Heros()
{

}

void Heros::setPosx(int posxvariation)
{
    m_x = posxvariation;
}

void Heros::setPosy(int posyvariation)
{
    m_y = posyvariation;
}

void Heros::varPos(int taillex, int tailley)
{
    setDeplacement(false); // avant chaque d�placement on initailise d�placement a false
    int variationX=m_x+m_vx;
    int variationY=m_y+m_vy;

    if (variationX<taillex && variationX >= 0)
    {
        setPosx(variationX);
        setDeplacement(true); // on indique que mario s'est d�plac�
    }

    if (variationY<tailley && variationY >= 0)
    {
        setPosy(variationY);
        setDeplacement(true);
    }
}

int Heros::getPosx() const
{
    return m_x;
}

int Heros::getPosy() const
{
    return m_y;
}
int Heros::getVx() const
{
    return m_vx;
}

int Heros::getVy() const
{
    return m_vy;
}
bool Heros::getDeplacement() const
{
    return m_deplacement;
}

/// Modifie la direction du mouvement du personnage
void Heros::setVx(signed int nouveauVX)
{
    if(nouveauVX ==1 || nouveauVX==0 || nouveauVX== -1)
    {
        m_vx=nouveauVX;
    }
}

void Heros::setVy(signed int nouveauVY)
{
    if(nouveauVY ==1 || nouveauVY==0 || nouveauVY== -1)
    {
        m_vy=nouveauVY;
    }
}

void Heros::setDeplacement(bool nouveau)
{
    m_deplacement=nouveau;
}

//methodes
void Heros::diriger_Mario(int direction)
{
    bool fin = false;
    while(fin == false && !key[KEY_ESC])
    {
        if (key[KEY_UP] || direction == 0)
        {
            setVy(-1);
            setVx(0);
            fin = true;
        }
        else if (key[KEY_DOWN] || direction == 1)
        {
            setVy(1);
            setVx(0);
            fin = true;
        }
        else if (key[KEY_LEFT] || direction == 2)
        {
            setVy(0);
            setVx(-1);
            fin = true;
        }
        else if (key[KEY_RIGHT] || direction == 3)
        {
            setVy(0);
            setVx(1);
            fin = true;
        }
    }

}


void Heros::resetvar()
{
    setVx(0);
    setVy(0);
}

void Heros::collisionsMario(Niveau& niv) /// g�re les collisions de Mario avec les blocs
{
    bool test = false;
    bool caseVide = false;
    std::vector<std::vector<int> > m_goals = niv.getGoals();
    ///si il y a un bloc poussable
    if(getVx() == 1 && getPosx()< niv.getTaillex()-2)
    {
        if((niv.getCaseMatrice(getPosy(), getPosx()+1) == 2 || niv.getCaseMatrice(getPosy(), getPosx()+1) == 3)
           && niv.getCaseMatrice(getPosy(), getPosx()+2) == 0 )
        {
            caseVide = true;
            niv.setCaseMatrice(getPosy(), getPosx()+1, 0);
            for(int i=0; i<niv.getNbGoals(); i++)
            {
                /// si l'emplacement est sur un checkpoint
                if(m_goals[i][0] == getPosy() && m_goals[i][1] == getPosx()+2)
                    test = true;
            }
            if(test == true)
                niv.setCaseMatrice(getPosy(), getPosx()+2, 3); ///on met un bloc plac�
            else if(test != true) niv.setCaseMatrice(getPosy(), getPosx()+2, 2); /// sinon on met un bloc normal

            varPos(niv.getTaillex(), niv.getTailley());
        }
        else if ((niv.getCaseMatrice(getPosy(), getPosx()+1) == 2 || niv.getCaseMatrice(getPosy(), getPosx()+1) == 3)
                 && (niv.getCaseMatrice(getPosy(), getPosx()+2) == 1 || niv.getCaseMatrice(getPosy(), getPosx()+2) == 2
                     || niv.getCaseMatrice(getPosy(), getPosx()+2) == 3 || niv.getCaseMatrice(getPosy(), getPosx()+2) == 6))
            caseVide = true;
    }

    else if(getVx() == -1 && getPosx() > 1)
    {
        if((niv.getCaseMatrice(getPosy(), getPosx()-1) == 2 || niv.getCaseMatrice(getPosy(), getPosx()-1) == 3)
           && niv.getCaseMatrice(getPosy(), getPosx()-2) == 0 )
        {
            caseVide = true;
            niv.setCaseMatrice(getPosy(), getPosx()-1, 0);
            for(int i=0; i<niv.getNbGoals(); i++)
            {
                /// si l'emplacement est sur un checkpoint
                if(m_goals[i][0] == getPosy() && m_goals[i][1] == getPosx()-2)
                    test = true;
            }
            if(test == true)
                niv.setCaseMatrice(getPosy(), getPosx()-2, 3); ///on met un bloc plac�
            else if(test!=true) niv.setCaseMatrice(getPosy(), getPosx()-2, 2); /// sinon on met un bloc normal

             varPos(niv.getTaillex(), niv.getTailley());
        }
        else if ((niv.getCaseMatrice(getPosy(), getPosx()-1) == 2 || niv.getCaseMatrice(getPosy(), getPosx()-1) == 3)
                 && (niv.getCaseMatrice(getPosy(), getPosx()-2) == 1 || niv.getCaseMatrice(getPosy(), getPosx()-2) == 2
                     || niv.getCaseMatrice(getPosy(), getPosx()-2) == 3 || niv.getCaseMatrice(getPosy(), getPosx()-2) == 6))
            caseVide = true;
    }

    else if(getVy() == 1 && getPosy() < niv.getTailley()-2)
    {
        if((niv.getCaseMatrice(getPosy()+1, getPosx()) == 2 || niv.getCaseMatrice(getPosy()+1, getPosx()) == 3)
           && niv.getCaseMatrice(getPosy()+2, getPosx()) == 0)
        {
            caseVide = true;
            niv.setCaseMatrice(getPosy()+1, getPosx(), 0);
            for(int i=0; i<niv.getNbGoals(); i++)
            {
                /// si l'emplacement est sur un checkpoint
                if(m_goals[i][0] == getPosy()+2 && m_goals[i][1] == getPosx())
                    test = true;
            }
            if(test == true)
                niv.setCaseMatrice(getPosy()+2, getPosx(), 3); ///on met un bloc plac�
            else if(test!=true)niv.setCaseMatrice(getPosy()+2, getPosx(), 2); /// sinon on met un bloc normal

            varPos(niv.getTaillex(), niv.getTailley());
        }
        else if ((niv.getCaseMatrice(getPosy()+1, getPosx()) == 2 || niv.getCaseMatrice(getPosy()+1, getPosx()) == 3)
                 && (niv.getCaseMatrice(getPosy()+2, getPosx()) == 1 || niv.getCaseMatrice(getPosy()+2, getPosx()) == 2
                     || niv.getCaseMatrice(getPosy()+2, getPosx()) == 3 || niv.getCaseMatrice(getPosy()+2, getPosx()) == 6))
            caseVide = true;
    }

    else if(getVy() == -1 && getPosy() > 1)
    {
        if((niv.getCaseMatrice(getPosy()-1, getPosx()) == 2 || niv.getCaseMatrice(getPosy()-1, getPosx()) == 3)
           && niv.getCaseMatrice(getPosy()-2, getPosx()) == 0)
        {
            caseVide = true;
            niv.setCaseMatrice(getPosy()-1, getPosx(), 0);
            for(int i=0; i<niv.getNbGoals(); i++)
            {
                /// si l'emplacement est sur un checkpoint
                if(m_goals[i][0] == getPosy()-2 && m_goals[i][1] == getPosx())
                    test = true;
            }
            if(test == true)
                niv.setCaseMatrice(getPosy()-2, getPosx(), 3); ///on met un bloc plac�
            else if(test!=true) niv.setCaseMatrice(getPosy()-2, getPosx(), 2); /// sinon on met un bloc normal

            varPos(niv.getTaillex(), niv.getTailley());
        }
        else if ((niv.getCaseMatrice(getPosy()-1, getPosx()) == 2 || niv.getCaseMatrice(getPosy()-1, getPosx()) == 3)
                 && (niv.getCaseMatrice(getPosy()-2, getPosx()) == 1 || niv.getCaseMatrice(getPosy()-2, getPosx()) == 2
                     || niv.getCaseMatrice(getPosy()-2, getPosx()) == 3 || niv.getCaseMatrice(getPosy()-2, getPosx()) == 6))
            caseVide = true;
    }

    if(caseVide == false)
    {
       /// diff�rents types de mouvements pour blindage sur les murs simples
        if((getVx() == 1 && getPosx() < niv.getTaillex()-1) || (getVx() == -1 && getPosx() > 0)
           || (getVy() == 1 && getPosy() < niv.getTailley()-1) || (getVy() == -1 && getPosy() > 0))
        {
            if ( niv.getCaseMatrice(getPosy() + getVy(), getPosx() + getVx()) !=1) // collision avec un mur
                varPos(niv.getTaillex(), niv.getTailley());
        }
    }
}

